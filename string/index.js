//                              _(\_/)
//                            ,((((^`\
//                           ((((  (6 \
//                         ,((((( ,    \
//     ,,,_              ,(((((  /"._  ,`,
//    ((((\\ ,...       ,((((   /    `-.-'
//    )))  ;'    `"'"'""((((   (
//   (((  /            (((      \
//    )) |                      |
//   ((  |        .       '     |
//   ))  \     _ '      `t   ,.')
//   (   |   y;- -,-""'"-.\   \/
//   )   / ./  ) /         `\  \
//      |./   ( (           / /'
//      ||     \\          //'|
//      ||      \\       _//'||
//      ||       ))     |_/  ||
//      \_\     |_/          ||
//      `'"                  \_\
//                           `'"
export default module.exports = {
    taughtHours: "Hours\nteached",
    daysOff: "Days off",
    overTime: "Over time",
    checkIn: "Check in",
    checkOut: "Check out",
    titleCheckIn: "Check-in",
    titleCheckOut: "Check-out",
    titleCalendar: "Calendar",
    titlePayroll: "Payroll",
    titleReview: "Review",
    titleChangePassword: "Change Password",
    titleEditProfile: "Edit Profile",
    textForgotPassword: "Forgot password?",
    hintUsername: "Username",
    hintPassword: "Password",
    textsend: "Send",
    titleFogotPassword: "Forgot password",
    titleAttendance: "Attendance",
    titleAbsent: "Leave of absent",
    titleNotCheck: "Not check",
    titleListToday: "Todays",
    titleListTeachers: "Teachers",
    titleTimeSheetDetail: "Timesheet",
    hintReview: "Enter your review.",
    textSubmit: "Submit",
    textTaughtHours: "Hours teached",
    textChangePass: "Change pass",
    textEditInfo: "Edit Info",
    textTimeSheet: "Time Sheet",
    textLogout: "Logout",
    hintCurrentPassword: "Current password",
    hintEmail: "Email",
    hintAdress: "Adress",
    hintNewPassword: "New password",
    hintReEnterPassword: "Re-enter password",
    hintPhoneNumber: "Phone number",
    textSave: "Save",
    textWarning: "Warning",
    messageAlert: "Not yet teaching, are you sure \ryou want to finish early",
    textCancel: "Cancel",
    textOk: "Ok",
    textValidate: "Hints on getting your new password right:\rYour new password must be between 8 and 50 characters in length.\rIt may contain punctuation(except \"), numbers, upper case letters and lower case letters.",
    textReset: "Reset",
    textSelectTheDay: "Select the day you want to off",
    textDone: "Done",
    textOffDays: "Off days",
    textTeacherOffToday: "Teacher off today",
    textNotChecked: "Not checked",
    textListToday: "List today",
    textTotalTeacher: "Total teacher",
    hintNoteSomeThings: "Note some things",
    textCall: "Call",
    textSelectTheDayToWork: "Select the day you want to work",

};