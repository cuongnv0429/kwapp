export default module.exports = {
    colorTransparent: '#00FFFFFF',
    colorBlack: '#000000',
    colorWhite: '#FFFFFF',
    colorBorder: '#cccccc',
}