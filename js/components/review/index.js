import React, {Component} from 'react';
import {View, Text, Image, ListView, TouchableOpacity, Dimensions, 
        TextInput, Platform, findNodeHandle, ScrollView, Keyboard} from 'react-native';
import {Container, Content, Header, Left, Body, Right, Button   } from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { setStatusTab } from '../../actions/tab';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const backIcon = require('../../../img/back.png');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
class Review extends Component {
    componentWillMount () { 
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this)); 
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this)); 
    };
    componentWillUnmount () { 
        this.keyboardDidShowListener.remove(); 
        this.keyboardDidHideListener.remove(); 
    };
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <Left>
                    <Button transparent onPress={() => {Actions.pop()}}>
                        <Image source={backIcon} style={{width: 20, height: 20}}/>
                    </Button>
                </Left>
                <Body>
                    <Text style={{color: 'white', fontSize: Dimens.dp_18}}>{Strings.titleReview}</Text>
                </Body>
                <Right />
                </Header>
                <ScrollView>
                <View style={{flex: 1}}>
                    <View style={{ margin: Dimens.dp_10, flexDirection: 'row',padding: Dimens.dp_15, backgroundColor: '#d3deff'}}>
                        <View style={{flex: 0.5, flexDirection: 'column', alignItems: 'center',paddingRight: 10, justifyContent: 'center'}}>
                            <Text style={{color: 'black', fontSize: Dimens.dp_20, fontWeight: 'bold'}}>KTH3 - AE3</Text>
                            <Text style={{color: 'black', fontSize: Dimens.dp_12, marginTop: Dimens.dp_10}}>07:30AM | 09:30AM</Text>
                        </View>

                        <View style={{flex: 0.5, alignItems: 'center', borderLeftColor: 'white', borderLeftWidth: 0.5}}>
                            <Text style={{fontSize: Dimens.dp_15, marginLeft: Dimens.dp_15}}>TIME TO KNOW</Text>
                        </View>
                    </View>
                    
                    <View style={{backgroundColor: '#e1e1e1', padding: Dimens.dp_15, height: deviceHeight * 0.4, alignItems: 'flex-start'}}>
                        <AutoGrowingTextInput autoFocus = {true} maxHeight = {deviceHeight * 0.4} 
                            placeholder={Strings.hintReview} underlineColorAndroid ='transparent'
                            />
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'row', marginTop: Dimens.dp_30,
                                 alignItems: 'center', justifyContent: 'center', marginBottom: deviceWidth / 5}}>
                            <Button rounded onPress ={() => {alert(1)}}
                            style={{backgroundColor: '#b51218', width: deviceWidth / 2,
                            height: deviceWidth / 6,
                            justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textSubmit.toLocaleUpperCase()}</Text></Button>
                    </View>
                </View>
                </ScrollView>
            </Container>
        );
    }

    _keyboardDidShow () { 
        this.props.setStatusTab(true);
    };
    _keyboardDidHide () { 
        this.props.setStatusTab(false);
    };
}

const bindActions = dispatch => ({
    setStatusTab: (isStatusTab) => dispatch(setStatusTab(isStatusTab))
});

const mapStateToProps = state =>({
    isStatusTab: state.tab.isStatusTab
});

export default connect(mapStateToProps, bindActions)(Review);