import React, {Component} from 'react';
import {View, Text, Image, TextInput, Dimensions} from 'react-native';
import {Container, Content, Header, Left, Button, Body, Right, Title} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
const imageBG = require('../../../img/bg_login.png');
const backIcon = require('../../../img/back.png');
const cancleIcon = require('../../../img/cancle.png');
const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const styles = require('./styles.js');

const deviceWith = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

export default class ChangePassword extends Component {
    render() {
        return (
            <Container>
                <View style={{flex: 1}}>
                    <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <Left>
                        <Button transparent onPress={() => {Actions.pop()}}>
                            <Image source={backIcon} style={{width: 20, height: 20}}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{color: 'white', fontSize: 18, width: deviceWith * 0.6}}>{Strings.titleChangePassword}</Title>
                    </Body>
                    <Right />
                    </Header>
                    <Content>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 10, 
                                width: deviceWith * 0.85, backgroundColor: '#EBEBEB', padding: Dimens.dp_10}}>
                                <Text style={{flex: 1, color: 'black', fontSize: Dimens.dp_12}}>Re-enter new password wrong</Text>
                                <Image source={cancleIcon} style={{width: Dimens.dp_20, height: Dimens.dp_20, marginLeft: 10}}></Image>
                            </View>
                            <View style={{width: deviceWith * 0.85, marginTop: Dimens.dp_20, borderBottomWidth: 1, 
                                borderBottomColor: '#6e799d', height: Dimens.dp_30}}>
                                <AutoGrowingTextInput multiline={false} placeholder={Strings.hintCurrentPassword} placeholderTextColor = 'black'
                                    underlineColorAndroid ='transparent'
                                    style={{fontSize: 14,height: 20, paddingLeft: 5}}></AutoGrowingTextInput>
                            </View>
                            <View style={{width: deviceWith * 0.85, marginTop: Dimens.dp_20, borderBottomWidth: 1, 
                                borderBottomColor: '#6e799d', height: Dimens.dp_30}}>
                                <AutoGrowingTextInput multiline={false} placeholder={Strings.hintNewPassword} placeholderTextColor = 'black'
                                    underlineColorAndroid ='transparent'
                                    style={{fontSize: 14,height: 20, paddingLeft: 5}}></AutoGrowingTextInput>
                            </View>
                            <View style={{width: deviceWith * 0.85, marginTop: Dimens.dp_20, borderBottomWidth: 1, 
                                borderBottomColor: '#6e799d', height: Dimens.dp_30, paddingLeft: 5}}>
                                <AutoGrowingTextInput multiline={false} placeholder={Strings.hintReEnterPassword} placeholderTextColor = 'black'
                                    underlineColorAndroid ='transparent'
                                    style={{fontSize: 14,height: 20}}></AutoGrowingTextInput>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 30, height: deviceHeight * 0.25,
                                    alignItems: 'center', justifyContent: 'center', marginBottom: deviceWith / 5}}>
                                <Button rounded onPress ={() => {alert(1)}}
                                style={{backgroundColor: '#b51218', width: deviceWith * 0.6,
                                height: deviceWith * 0.1,
                                justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.titleChangePassword.toLocaleUpperCase()}</Text></Button>
                            </View>
                            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{color: 'black', padding: Dimens.dp_15}}>{Strings.textValidate}</Text>
                            </View>
                        </View>
                        
                    </Content>
                </View>
            </Container>
        );
    }
}