import React, {Component} from 'react';
import {View, Text, Image, ListView, StyleSheet} from 'react-native';
import {Container, Content, Header} from 'native-base';
import CheckBox from 'react-native-check-box'

const backIcon = require('../../../img/back.png');
const Dimens = require('../../../dimen');
const Strings = require('../../../string');
const checkIcon = require('../../../img/check.png');
const unCheckIcon = require('../../../img/not_check.png');
var data = [];
var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const styles = StyleSheet.create({
    rightTextStyle : {
        fontSize: 10,
        color: 'red'
    }
})
export default class TimeSheetDetail extends Component {

    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }
        this.state = ({
            dataSource: ds.cloneWithRows(data)
        });
    }
    _renderRow(rowData, sectionId, rowId) {
        return(
            <View style={{flex: 1, flexDirection: 'row', borderBottomColor: '#cccccc', borderBottomWidth: 0.5,
                 height: Dimens.dp_50}}>
                <View style={{flex: 0.4, justifyContent: 'center'}}>
                    <Text style={{paddingLeft: Dimens.dp_20}}>Mon (2017-5-10)</Text>
                </View>
                <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
                    <CheckBox
                        isChecked={true}
                        onClick={()=>console.log(data)}
                        checkedImage={<Image source={checkIcon} style={{width: Dimens.nomalIcon,
                        height: Dimens.nomalIcon}}></Image>}
                        unCheckedImage={<Image source={unCheckIcon} style={{width: Dimens.nomalIcon,
                        height: Dimens.nomalIcon}}></Image>}/>
                </View>
                <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
                    <CheckBox
                        isChecked={true}
                        onClick={()=>console.log(data)}
                        checkedImage={<Image source={checkIcon} style={{width: Dimens.nomalIcon,
                        height: Dimens.nomalIcon}}></Image>}
                        unCheckedImage={<Image source={unCheckIcon} style={{width: Dimens.nomalIcon,
                        height: Dimens.nomalIcon}}></Image>}/>
                </View>
                <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
                    <CheckBox
                        isChecked={true}
                        onClick={()=>console.log(data)}
                        checkedImage={<Image source={checkIcon} style={{width: Dimens.nomalIcon,
                        height: Dimens.nomalIcon}}></Image>}
                        unCheckedImage={<Image source={unCheckIcon} style={{width: Dimens.nomalIcon,
                        height: Dimens.nomalIcon}}></Image>}/>
                </View>
            </View>
        );
    }
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 0.1, alignItems: 'center'}}>
                            <Image source={backIcon} style={{width: Dimens.smallIcon, height: Dimens.smallIcon, resizeMode: 'contain'}}/>
                        </View>
                        <View style={{flex: 0.9, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: Dimens.dp_18}}>{Strings.titleTimeSheetDetail}</Text>
                        </View>
                        <View style={{flex: 0.1, alignItems: 'center'}}/>
                    </View>
                </Header>
                <View style={{flex: 1}}>
                    <View style={{flexDirection: 'row', height: Dimens.dp_50}}>
                        <View style={{flex: 0.4, justifyContent: 'center'}}>
                            <Text style={{color: 'black', fontSize: Dimens.dp_18, paddingLeft: Dimens.dp_20}}>Days</Text>
                        </View>
                        <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
                            <CheckBox
                                isChecked={true}
                                rightTextView={<Text style={{paddingLeft: 5, color: 'black'}}>All C1</Text>}
                                onClick={()=>console.log(data)}
                                checkedImage={<Image source={checkIcon} style={{width: Dimens.nomalIcon,
                                height: Dimens.nomalIcon}}></Image>}
                                unCheckedImage={<Image source={unCheckIcon} style={{width: Dimens.nomalIcon,
                                height: Dimens.nomalIcon}}></Image>}/>
                        </View>
                        <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
                            <CheckBox
                                isChecked={true}
                                rightTextView={<Text style={{paddingLeft: 5, color: 'black'}}>All C2</Text>}
                                onClick={()=>console.log(data)}
                                checkedImage={<Image source={checkIcon} style={{width: Dimens.nomalIcon,
                                height: Dimens.nomalIcon}}></Image>}
                                unCheckedImage={<Image source={unCheckIcon} style={{width: Dimens.nomalIcon,
                                height: Dimens.nomalIcon}}></Image>}/>
                        </View>
                        <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
                            <CheckBox
                                rightTextView={<Text style={{paddingLeft: 5, color: 'black'}}>All C3</Text>}
                                isChecked={true}
                                onClick={()=>console.log(data)}
                                checkedImage={<Image source={checkIcon} style={{width: Dimens.nomalIcon,
                                height: Dimens.nomalIcon}}></Image>}
                                unCheckedImage={<Image source={unCheckIcon} style={{width: Dimens.nomalIcon,
                                height: Dimens.nomalIcon}}></Image>}/>
                        </View>
                    </View>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this._renderRow}/>
                </View>
            </Container>
        );
    }
}