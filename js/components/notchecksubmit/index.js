import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Actions, ActionConst} from 'react-native-router-flux';
import {Text, Image, View, Dimensions, ListView} from 'react-native';
import { Container, Button, H3, Header, Left, Body, Title, Right, Content} from 'native-base';

const backIcon = require('../../../img/back.png');
const teacherIcon = require('../../../img/teacher.png');
const studentIcon = require('../../../img/student.png');
const locationIcon = require('../../../img/location.png');
const deviceWidth = Dimensions.get('window').width;
const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
var data = [];
class NotCheckSubmit extends Component {
    constructor(props) {
        super(props);
        if (data.length < 1){
            for (var i = 0; i < 3; i++) {
                data.push(i);
            }
        }
        
        this.state = {
            datasource: ds.cloneWithRows(data)
        };
    }
    componentWillUnMount() {
        
        
    }
    _renderRow(rowData, rowId, sectionsId) {
        return (
            <Text style={{color: 'black', fontSize: 15, marginTop: 10}}>{rowData}</Text>
        )
    }
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 0.1, alignItems: 'center'}}>
                            <Button transparent onPress={() => {Actions.pop()}}>
                                <Image source={backIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}/>
                            </Button>
                        </View>
                        <View style={{flex: 0.8, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: Dimens.titleToolbar}}>
                                Nguyen Van A
                            </Text>
                        </View>
                        <View style={{flex: 0.1}}/>
                    </View>
                </Header>
                <Content>
                    <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: 16, color: 'black', fontWeight: 'bold', marginTop: 10}}>KTH3 - AE3</Text>
                        <Text style={{fontSize: 14, marginTop: 15, color: 'black'}}>09:30AM | 10:30AM</Text>
                        <Text style={{fontSize: 16, color: 'black', marginTop: 15}}>ENGLISH FOR AGES 4-6 YEARS</Text>
                        <View style={{width: deviceWidth - 40, margin: Dimens.dp_20, backgroundColor: '#d3deff', padding: 10}}>
                            <View style={{flexDirection: 'row', borderBottomColor: 'white', borderBottomWidth: 0.5}}>
                                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, 
                                borderRightWidth: 0.5, borderRightColor: 'white'}}>
                                    <Image source={teacherIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                                </View>
                                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center', marginBottom: 10}}>
                                    <Image source={studentIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{flex: 0.5}}>
                                    <ListView
                                    dataSource={this.state.datasource}
                                    renderRow={this._renderRow}></ListView>
                                </View>  
                                <View style={{flex: 0.5, borderLeftWidth: 0.5, borderLeftColor: 'white', 
                                    marginTop: 10, alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={{color: 'black'}}> 15 STUDENT</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row', marginLeft: Dimens.dp_20, marginRight: Dimens.dp_20, marginBottom: Dimens.dp_20}}>
                            <View style={{flex: 0.35, backgroundColor: '#b51218', padding: Dimens.dp_5, marginRight: Dimens.dp_15, justifyContent: 'center'}}>
                                <Text style={{color: 'white', fontSize: Dimens.nomalText}}>IN 07:20AM</Text>
                            </View>
                            <View style={{flex: 0.65}}>
                                <Text style={{color: 'black', fontSize: Dimens.nomalText}} numberOfLines={2} ellipsizeMode='tail' lineBreakMode='tail'>
                                    42/14 Nguyen Huu Tien street, Tay Thanh ward, Tan Phu district, HCMC
                                </Text>
                            </View>
                            
                        </View>
                        <View style={{flexDirection: 'row', marginLeft: Dimens.dp_20, marginRight: Dimens.dp_20, marginBottom: Dimens.dp_20}}>
                            <View style={{flex: 0.35, backgroundColor: '#b51218', padding: Dimens.dp_5, marginRight: Dimens.dp_15, justifyContent: 'center'}}>
                                <Text style={{color: 'white', fontSize: Dimens.nomalText}}>OUT 09:40 AM</Text>
                            </View>
                            <View style={{flex: 0.65}}>
                                <Text style={{color: 'black', fontSize: Dimens.nomalText}} numberOfLines={2} ellipsizeMode='tail' lineBreakMode='tail'>
                                    42/14 Nguyen Huu Tien street, Tay Thanh ward, Tan Phu district, HCMC
                                </Text>
                            </View>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 20,marginBottom: 20}}>
                        <View style={{flex: 0.5, flexDirection: 'row', width: deviceWidth * 0.3, alignItems: 'center', justifyContent: 'center'}}>
                              <Button rounded onPress ={() => {alert(1)}}
                              style={{backgroundColor: '#18285a', width: deviceWidth * 0.3,
                              height: deviceWidth * 0.1,
                              justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textCall.toLocaleUpperCase()}</Text></Button>
                        </View>

                        <View style={{flex: 0.5, flexDirection: 'row', width: deviceWidth * 0.3, alignItems: 'center', justifyContent: 'center'}}>
                              <Button rounded onPress ={() => {alert(1)}}
                              style={{backgroundColor: '#b51218', width: deviceWidth * 0.3,
                              height: deviceWidth * 0.1,
                              justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textSubmit.toLocaleUpperCase()}</Text></Button>
                        </View>
                  </View>
                    </View>
                </Content>
            </Container>
        );
    }

    _goToAttendance() {
        Actions.attendance({type: ActionConst.PUSH});
    }
}

function bindActions(distpatch) {
    return {

    };
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, bindActions) (NotCheckSubmit);