import React, {Component} from 'react';
import {View, Text, ListView, Image, Dimensions, TouchableOpacity} from 'react-native';
import {Container, Content, Header, Button} from 'native-base';
import {Actions, ActionConst} from 'react-native-router-flux';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const checkIcon = require('../../../img/check.png');
const notCheckIcon = require('../../../img/not_check.png');
const deviceWidth = Dimensions.get('window').width;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
var data = [];
export default class AbsentDetail extends Component {
    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }
        this.state = {
            dataSource: ds.cloneWithRows(data)
        }
    }

    _renderRow(rowData, sectionId, rowId) {
        return (
            <View style={{flex: 1, alignItems: 'center', marginTop: Dimens.dp_15}}>
            <TouchableOpacity>
            <View style={{width: deviceWidth * 0.9, alignItems: 'center',justifyContent: 'center',  flexDirection: 'row', backgroundColor: '#DADADA', 
                borderRadius: Dimens.dp_20, paddingTop: Dimens.dp_10, paddingBottom: Dimens.dp_10, }}>
                <Image source={checkIcon} style={{width: Dimens.dp_30, height: Dimens.dp_30, marginLeft: Dimens.dp_5, marginRight: Dimens.dp_10}}/>
                <View style={{flex: 1, flexDirection: 'column'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 0.5, justifyContent: 'center'}}>
                            <Text style={{color: 'black', fontSize: Dimens.dp_15}}>KTH-AE3</Text>
                        </View>
                        <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'black', fontSize: Dimens.dp_12}}>07:30AM|09:30AM</Text>
                        </View>
                    </View>
                    <View style={{marginRight: Dimens.dp_20}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_15}}>Nguyen Thi Thap</Text>
                    </View>
                </View>
            </View>
            </TouchableOpacity>
            </View>
            
        );
    }

    render() {
        return(
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View>
                            <Button transparent onPress={() => {Actions.pop()}}>
                                <Text style={{color: 'white', fontSize: Dimens.dp_12}}>{Strings.textCancel}</Text>
                            </Button>
                        </View>
                        <View style={{flex: 0.9, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: 18}}>{Strings.titleAbsent}</Text>
                        </View>
                        <View>
                            <Button transparent onPress={() => {Actions.pop()}}>
                                <Text style={{color: 'white', fontSize: Dimens.dp_12}}>{Strings.textDone}</Text>
                            </Button>
                        </View>
                    </View>
                </Header>
                <Content>
                    <View style={{alignItems: 'center', justifyContent: 'center', marginTop: Dimens.dp_20, marginBottom: Dimens.dp_20}}>
                        <Text style={{color: '#18285a', fontSize: Dimens.dp_18}}>15-03-2017</Text>
                    </View>
                    <ListView
                        renderRow={this._renderRow}
                        dataSource={this.state.dataSource}>
                    </ListView>
                    <View style={{height: 50, backgroundColor: 'transparent'}}></View>
                </Content>
            </Container>
        );
    }
}