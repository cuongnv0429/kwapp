import React, {Component} from 'react';
import {View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import {Container, Content, Button} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';
const backIcon = require('../../../img/back.png');
const logoutIcon = require('../../../img/logout.png');
const mailIcon = require('../../../img/mail.png');
const locationIcon = require('../../../img/location.png');
const birthdayIcon = require('../../../img/birthday.png');
const phoneProfileIcon = require('../../../img/phone_profile.png');
const clockProfileIcon = require('../../../img/clock_profile.png');
const editProfileIcon = require('../../../img/edit_profile.png');
const timeSheetIcon = require('../../../img/time_sheet.png');
const bgDashboard = require('../../../img/bg_dashboard.png');
const lineProfileIcon = require('../../../img/line_profile.png');
const avatarProfileIcon = require('../../../img/avatar_profile.png');
const Dimens = require('../../../dimen');
const Strings = require('../../../string');
const styles = require('./styles.js');
const deviceWidth = Dimensions.get('window').width;

export default class Profile extends Component {
    render() {
        return (
            <Container>
                
                <Image  source={bgDashboard} style={{resizeMode: 'stretch', flex: 1, width: null, height: null}}>
                <View style={{flex: 1, backgroundColor: 'rgba(255, 255, 255, 0.6)'}}>
                <Content>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <View style={{flex: 0.6}}>
                            <View style={{flex: 1, height: 24}}></View>
                            <View style={{flex: 1, height: 30,justifyContent: 'center', alignItems: 'center'}}>
                                <Button transparent onPress={() => {Actions.pop()}}>
                                    <Image source={backIcon} style={{width: Dimens.dp_15, height: Dimens.dp_20}}/>
                                </Button>
                            </View>
                            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                <Image source={avatarProfileIcon} 
                                    style={{width: Dimens.dp_100, height: Dimens.dp_100, borderColor: 'white', borderRadius: 50, borderWidth: 2}}></Image>
                            </View>
                            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: Dimens.dp_15}}>
                                <Text style={{color: 'black', fontSize: Dimens.dp_18}}>Nguyen Van Cuong</Text>
                            </View>
                            <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
                                <Image source={lineProfileIcon} style={{resizeMode: 'contain', width: deviceWidth}}></Image>
                            </View>
                            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{color: 'black', fontSize: Dimens.dp_15}}>Professional Quality</Text>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: Dimens.dp_20, borderBottomColor: '#d6d6d6', borderBottomWidth: 2}}>
                            <View style={{flex: 0.3}}>
                                <TouchableOpacity onPress={() => {Actions.changepassword()}}>
                                 <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center',
                                 padding: Dimens.dp_10, backgroundColor: 'white'}}>
                                    <Image source={clockProfileIcon} style={{width: Dimens.dp_25, height: Dimens.dp_25, marginBottom: Dimens.dp_10, resizeMode: 'contain'}}></Image>
                                    <Text style={{color: 'black', fontSize: Dimens.dp_12}}>{Strings.textChangePass}</Text>
                                </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex: 0.3, borderLeftWidth: 2, borderLeftColor: '#d6d6d6'}}>
                                <TouchableOpacity onPress={() => {Actions.editprofile()}}>
                                 <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center',
                                 padding: Dimens.dp_10, backgroundColor: 'white'}}>
                                    <Image source={editProfileIcon} style={{width: Dimens.dp_25, height: Dimens.dp_25, marginBottom: Dimens.dp_10, resizeMode: 'contain'}}></Image>
                                    <Text style={{color: 'black', fontSize: Dimens.dp_12}}>{Strings.textEditInfo}</Text>
                                </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex: 0.3, borderLeftWidth: 2, borderLeftColor: '#d6d6d6'}}>
                                <TouchableOpacity onPress={() => {Actions.timesheet()}}>
                                 <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center',
                                 padding: Dimens.dp_10, backgroundColor: 'white'}}>
                                    <Image source={timeSheetIcon} style={{width: Dimens.dp_25, height: Dimens.dp_25, marginBottom: Dimens.dp_10, resizeMode: 'contain'}}></Image>
                                    <Text style={{color: 'black', fontSize: Dimens.dp_12}}>{Strings.textTimeSheet}</Text>
                                </View>
                                </TouchableOpacity>
                            </View>
                            
                        </View>
                        <View style={{flexDirection: 'column', marginTop: Dimens.dp_20, backgroundColor: 'transparent', paddingLeft: Dimens.dp_30}}>
                            <View style={{flexDirection:'row', alignItems: 'center', height: 50}}>
                                <Image source={birthdayIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}></Image>
                                <Text numberOfLines={1} ellipsizeMode='tail' style={{color: 'black', fontSize: Dimens.dp_15, marginLeft: Dimens.dp_20}}>13-10-1986</Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems: 'center', height: 50}}>
                                <Image source={phoneProfileIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}></Image>
                                <Text numberOfLines={1} ellipsizeMode='tail' style={{color: 'black', fontSize: Dimens.dp_15, marginLeft: Dimens.dp_20}}>0936 777 908</Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems: 'center', height: 50}}>
                                <Image source={mailIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}></Image>
                                <Text numberOfLines={1} ellipsizeMode='tail' style={{color: 'black', fontSize: Dimens.dp_15, marginLeft: Dimens.dp_20}}>nguyenvanz@gmail.com</Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems: 'center', height: 50}}>
                                <Image source={locationIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}></Image>
                                <Text numberOfLines={1} ellipsizeMode='tail' style={{color: 'black', fontSize: Dimens.dp_15, marginLeft: Dimens.dp_20}}>13-10-1986</Text>
                            </View>
                        </View>
                        <View style={{alignItems: 'center', justifyContent: 'center', marginTop: Dimens.dp_40, marginBottom: Dimens.dp_30}}>
                            <View style={{flexDirection: 'row', justifyContent: 'center', paddingBottom: Dimens.dp_10, paddingTop: Dimens.dp_10, 
                                paddingLeft: Dimens.dp_15, paddingRight: Dimens.dp_15,
                                alignItems: 'center', backgroundColor: '#18285a', borderRadius: Dimens.dp_10}}>
                                <Image source={logoutIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}></Image>
                                <Text style={{color: 'white', fontSize: Dimens.nomalText, marginLeft: Dimens.dp_20}}>{Strings.textLogout}</Text>
                            </View>
                        </View>
                    </View>
                </Content>
                </View>
                </Image>
                
            </Container>
        );
    }
}