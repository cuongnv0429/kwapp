const React = require('react-native');
const {StyleSheet, Dimensions, PixelRatio} = React;


const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
    avatar: {
        height: 60,
        width: 60,
        borderRadius: 30,
        borderWidth: 2,
        borderColor: 'red'
    },
    content: {
        
    }
}