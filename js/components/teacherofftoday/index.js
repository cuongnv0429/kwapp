import React, {Component} from 'react';
import { Image, View, StatusBar, ListView, Dimensions, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { Container, Button, Text, Header, Left, Body, Title, Right} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const backIcon = require('../../../img/back.png');
var data = [];
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
const deviceWidth = Dimensions.get('window').width;
export default class TeacherOffToday extends Component {

    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }
        this.state = {
            dataSource: ds.cloneWithRows(data)
        }
    }

    _renderRow(rowData, sectionId, rowId) {
        return (
            <View style={{padding: Dimens.dp_10, borderRadius: Dimens.dp_15, backgroundColor: '#ECECEC', marginBottom: Dimens.dp_15}}>
                <Text style={{color: 'black', fontSize: Dimens.dp_15}}>Nguyen Van Z</Text>
                <View style={{flex: 1, flexDirection: 'row', marginTop: Dimens.dp_20, alignItems: 'center'}}>
                    <TouchableOpacity onPress={() => {this._onClickItem(rowId)}}>
                    <Text style={{padding: Dimens.dp_10, backgroundColor: '#b51218', color: 'white', 
                    fontSize: Dimens.dp_15}}>CHANGE SCHEDULE</Text>
                    </TouchableOpacity>
                    <Text style={{color: 'black', fontSize: Dimens.dp_15, marginLeft: Dimens.dp_20}}>15-03-2017</Text>
                </View>
            </View>
        );
    }

    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 0.1}}>
                        <Button transparent onPress={() => {Actions.pop()}}>
                            <Image source={backIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}></Image>
                        </Button>
                    </View>
                    <View style={{flex: 0.9, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'white', fontSize: 18}}>{Strings.textTeacherOffToday}</Text>
                    </View>
                    <View style={{flex: 0.1}}>
                        
                    </View>
                </View>
                </Header>
                <View style={{flex: 1}}>
                    <ListView
                        contentContainerStyle={{padding: Dimens.dp_15}}
                        renderRow={this._renderRow.bind(this)}
                        dataSource={this.state.dataSource}>

                    </ListView>
                </View>
            </Container>
        );
    }
    _onClickItem(rowId) {
        Actions.teacherofftodaydetail();
    }
}