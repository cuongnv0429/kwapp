import React, {Component} from 'react';
import {View, Text, ListView, Dimensions, Image, TouchableOpacity} from 'react-native';
import {Container, Content, Header, Button} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const backIcon = require('../../../img/back.png');
const locationIcon = require('../../../img/location.png');
const dashboardBg = require('../../../img/bg_dashboard.png');
var data = [];
const deviceWidth = Dimensions.get('window').width;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});

export default class NotCheck extends Component {

    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }
        this.state = ({
            dataSource: ds.cloneWithRows(data)
        });
    }

    _renderRow(rowData, sectionId, rowId) {
        return (
            <TouchableOpacity onPress={() => {this._onClickItem(rowId)}}>
            <View style={{width: deviceWidth * 0.9, flexDirection: 'row', padding: Dimens.dp_15, 
                backgroundColor: 'rgba(255, 255, 255, 0.5)', marginTop: Dimens.dp_20, borderRadius: Dimens.dp_15}}>
                <View style={{flex: 0.8, flexDirection: 'column'}}>
                    <Text style={{color: 'black', fontSize: Dimens.dp_18, marginBottom: Dimens.dp_10}}>Nguyen Van A</Text>
                    <View style={{flex: 1, flexDirection: 'row', marginBottom: Dimens.dp_10}}>
                        <Text style={{color: 'black', fontSize: Dimens.nomalText, marginRight: Dimens.dp_10}}>KTH3-AE3</Text>
                        <Text style={{color: 'black', fontSize: Dimens.nomalText}}>Nguyen Van A</Text>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <Text style={{paddingLeft: Dimens.dp_10, paddingRight: Dimens.dp_10, paddingTop: Dimens.dp_5,
                         paddingBottom: Dimens.dp_5, color: 'white', backgroundColor: '#464646', fontSize: Dimens.nomalText, marginRight: Dimens.dp_15}}>IN --- AM</Text>
                        <Text style={{paddingLeft: Dimens.dp_10, paddingRight: Dimens.dp_10, paddingTop: Dimens.dp_5,
                         paddingBottom: Dimens.dp_5, color: 'white', backgroundColor: '#464646', fontSize: Dimens.nomalText}}>OUT --- AM</Text>
                    </View>
                </View>
                <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={locationIcon} style={{width: Dimens.dp_30, height: Dimens.dp_30, resizeMode: 'contain'}}/>
                </View>
            </View>
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 0.1, alignItems: 'center'}}>
                            <Button transparent onPress={() => {Actions.pop()}}>
                                <Image source={backIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}/>
                            </Button>
                        </View>
                        <View style={{flex: 0.8, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: Dimens.titleToolbar}}>
                                {Strings.titleNotCheck}
                            </Text>
                        </View>
                        <View style={{flex: 0.1}}/>
                    </View>
                </Header>
                <Image source={dashboardBg} style={{flex: 1,
                    height: null, 
                    width: null, 
                    resizeMode: 'stretch' }}>
                    <View style={{flex: 1}}>
                        <ListView
                            contentContainerStyle={{alignItems: 'center'}}
                            renderRow={this._renderRow.bind(this)}
                            dataSource={this.state.dataSource}>
                        </ListView>     
                    </View>
                </Image>
            </Container>
        );
    }

    _onClickItem(rowId) {
        if (rowId == '0') {
            Actions.notchecksubmit();
        } else {
            Actions.notcheckreview();
        }
    }
}