import React, {Component} from 'react';
import {Text, Image, View, TextInput, Dimensions, 
    TouchableOpacity, Platform} from 'react-native';
import {connect} from 'react-redux';
import {Container, InputGroup, Input, Icon, Button, Content} from 'native-base';
import styles from './styles';
import { Actions, ActionConst} from 'react-native-router-flux';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';


const loginBg = require('../../../img/bg_login.png');
const loginLogo = require('../../../img/logo.png');
const usernameIcon = require('../../../img/mail.png');
const passwordIcon = require('../../../img/clock.png');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const Colors = require('../../../color');
class Login extends Component {

    constructor(props) {
        super(props);
        this.state = ({
            username: ''
        });
    }

    render() {
        return (
            <Container>
                <Image source={loginBg} style={styles.imageBg}>
                    <Content>
                    <View style={styles.logoContainer}>
                        <Image source={loginLogo} style={styles.logo}></Image>
                    </View>
                    <View style={{flex: 0.3, marginLeft: 30, marginRight: 30, flexDirection: 'column', marginTop: deviceHeight/7}}>
                        {/*<View style={{
                            alignItems: 'center', flexDirection: 'row', height: 30}}>
                            <Image source={usernameIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                            <AutoGrowingTextInput multiline={false} placeholder={Strings.hintUsername} placeholderTextColor = 'black'
                                    underlineColorAndroid ='transparent'
                                    minHeight={Dimens.dp_40}
                                    maxHeight={Dimens.dp_45}
                                    onChange={(text) => this.setState({username: text})}
                                    style={{marginLeft: Platform.OS === 'android' ? Dimens.dp_5 : Dimens.dp_10, flex: 1, fontSize: 14}}></AutoGrowingTextInput>
                        </View>*/}
                        <View style={{justifyContent: 'center',
                            alignItems: 'center', flexDirection: 'row'}}>
                            <Image source={usernameIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                            <TextInput
                                style={{flex: 1, height: 30, lineHeight: 5, backgroundColor: 'red',
                                 textAlignVertical:'center'}}
                                onChangeText={(text) => this.setState({text})}
                                numberOfLines = {1}
                                placeholder={Strings.hintUsername} placeholderTextColor = 'black'
                                underlineColorAndroid ='transparent'
                                value={this.state.text}
                            />
                            {/*<Input style={{flex: 1, height: 30, backgroundColor: 'red', lineHeight: 10}} 
                                placeholder={Strings.hintUsername}
                                autoCapitalize="sentences"
                                keyboardType="default">
                            </Input>*/}
                        </View>
                        <View style={{height: 1, backgroundColor: Colors.colorBorder}}/>
                        <View style={{marginTop: 20,
                            alignItems: 'center', flexDirection: 'row', height: 30}}>
                            <Image source={passwordIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                            <AutoGrowingTextInput multiline={false} placeholder={Strings.hintPassword} placeholderTextColor = 'black'
                                    underlineColorAndroid ='transparent'
                                    minHeight={40}
                                    maxHeight={45}
                                    style={{marginLeft: Dimens.dp_10, flex: 1, fontSize: 14,
                                    marginBottom: Platform.OS == 'android' ? 10 : 0}}></AutoGrowingTextInput>
                        </View>
                        <View style={{height: 1, backgroundColor: Colors.colorBorder}}/>
                        <View style={{flex: 1, flexDirection: 'row', marginTop: 30,
                                 alignItems: 'center', justifyContent: 'center'}}>
                            <Button rounded onPress ={() => {this._login()}}
                            style={{backgroundColor: '#b51218', width: deviceWidth/2,
                            justifyContent: 'center', borderRadius: 10, height: 30}}><Text style={{color: 'white'}}>Login</Text></Button>
                        </View>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: Dimens.dp_30, marginBottom: 100}}>
                            <TouchableOpacity onPress={() => {this._gotoForgotPassword()}}>
                                <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>{Strings.textForgotPassword}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{flex: 0.5}}></View>
                    </Content>
                </Image>
            </Container>
        );
    }
    _gotoForgotPassword() {
        Actions.forgotpassword({type: ActionConst.PUSH});
    }
    _login() {
        if (this.state.username != '') {
            Actions.tabbar({type: ActionConst.REPLACE});
        } else {
            Actions.dashboardsupervisor({type: ActionConst.REPLACE});
        }
    }
}

function bindActions(dispatch) {
    return {

    };
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, bindActions)(Login);