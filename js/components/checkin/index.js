import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Actions, ActionConst} from 'react-native-router-flux';
import {Text, Image, View, Dimensions, ListView} from 'react-native';
import { Container, Button, H3, Header, Left, Body, Title, Right, Content} from 'native-base';

const backIcon = require('../../../img/back.png');
const teacherIcon = require('../../../img/teacher.png');
const studentIcon = require('../../../img/student.png');
const locationIcon = require('../../../img/location.png');
const deviceWith = Dimensions.get('window').width;
const Strings = require('../../../string');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
var data = [];
class CheckIn extends Component {
    constructor(props) {
        super(props);
        if (data.length < 1){
            for (var i = 0; i < 3; i++) {
                data.push(i);
            }
        }
        
        this.state = {
            datasource: ds.cloneWithRows(data)
        };
    }
    componentWillUnMount() {
        
        
    }
    _renderRow(rowData, rowId, sectionsId) {
        return (
            <Text style={{color: 'black', fontSize: 15, marginTop: 10}}>{rowData}</Text>
        )
    }
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <Left>
                    <Button transparent onPress={() => {Actions.pop()}}>
                        <Image source={backIcon} style={{width: 20, height: 20}}/>
                    </Button>
                </Left>
                <Body>
                    <Text style={{color: 'white', fontSize: 18}}>{Strings.titleCheckIn}</Text>
                </Body>
                <Right />
                </Header>
                <Content>
                    <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: 16, color: 'black', fontWeight: 'bold', marginTop: 10}}>KTH3 - AE3</Text>
                        <Text style={{fontSize: 14, marginTop: 15, color: 'black'}}>09:30AM | 10:30AM</Text>
                        <Text style={{fontSize: 16, color: 'black', marginTop: 15}}>ENGLISH FOR AGES 4-6 YEARS</Text>
                        <View style={{flex: 1,width: deviceWith * 0.8,marginTop: 20, backgroundColor: '#d3deff', padding: 10}}>
                            <View style={{flexDirection: 'row', borderBottomColor: 'white', borderBottomWidth: 0.5}}>
                                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center', marginBottom: 10, 
                                borderRightWidth: 0.5, borderRightColor: 'white'}}>
                                    <Image source={teacherIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                                </View>
                                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center', marginBottom: 10}}>
                                    <Image source={studentIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{flex: 0.5}}>
                                    <ListView
                                    dataSource={this.state.datasource}
                                    renderRow={this._renderRow}></ListView>
                                </View>  
                                <View style={{flex: 0.5, borderLeftWidth: 0.5, borderLeftColor: 'white', 
                                    marginTop: 10, alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={{color: 'black'}}> 15 STUDENT</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', marginTop: 20, width: deviceWith * 0.8,
                             alignItems: 'flex-start', justifyContent: 'flex-start'}}>
                            <Image source={locationIcon} style={{width: 15, height: 15,marginLeft: 10, marginRight: 15, resizeMode: 'contain'}}></Image>
                            <Text style={{color: 'black'}}>VATC NGUYEN THI THAP</Text>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', marginTop: 30,
                                 alignItems: 'center', justifyContent: 'center', marginBottom: deviceWith / 5}}>
                            <Button rounded onPress ={() => {this._goToAttendance()}}
                            style={{backgroundColor: '#b51218', width: deviceWith / 2,
                            height: deviceWith / 6,
                            justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.checkIn.toLocaleUpperCase()}</Text></Button>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }

    _goToAttendance() {
        Actions.attendance({type: ActionConst.PUSH});
    }
}

function bindActions(distpatch) {
    return {

    };
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, bindActions) (CheckIn);