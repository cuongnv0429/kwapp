import React, { Component } from 'react';
import { Image, View, StatusBar, ListView, Dimensions, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { Container, Button, H3, Text, Header, Left, Body, Title, Right} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';

const menuIcon = require('../../../img/menu.png');
const gs1Icon = require('../../../img/gs1.png');
const gs2Icon = require('../../../img/gs2.png');
const gs3Icon = require('../../../img/gs3.png');
const gs4Icon = require('../../../img/gs4.png');
const dashboardBg = require('../../../img/bg_dashboard.png');
const Dimens = require('../../../dimen');
const Strings = require('../../../string');
const deviceWidth = Dimensions.get('window').width;
var data = [];
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
export default class DashboardSupervisor extends Component {

    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 4; i++) {
                data.push(i);
            }
        }
        this.state = {
            dataSource: ds.cloneWithRows(data)
        }
    }
    _renderRow(rowData, sectiondId, rowId) {
        var content = '';
        var icon = '';
        var colorLine = '#39497a';
        switch (rowId) {
            case '0':
                icon = gs1Icon;
                content = Strings.textNotChecked;
                colorLine = '#b51218';
                break;
            case '1':
                icon = gs2Icon;
                content = Strings.textTeacherOffToday;
                break;
            case'2':
                icon = gs3Icon;
                content = Strings.textListToday;
                break;
            case '3':
                icon = gs4Icon;
                colorLine = '#b51218';
                content = Strings.textTotalTeacher;
                break;
        }
        return (
            <View style={{width: deviceWidth * 0.4, height: deviceWidth * 0.4, backgroundColor: 'rgba(255, 255, 255, 0.5)',
                marginBottom: 10, marginRight: 10, borderRadius: Dimens.dp_15, borderColor: 'white', borderWidth: 1}}>
            <TouchableOpacity onPress={() => {this._onClickItem(rowId)}}>
            <View style={{paddingLeft: Dimens.dp_15, width: deviceWidth * 0.4, height: deviceWidth * 0.4, justifyContent: 'center'}}>
                <View style={{backgroundColor: colorLine, height: 3, width: deviceWidth * 0.2, marginBottom: Dimens.dp_10}}></View>
                <Text style={{color: 'black', fontSize: Dimens.dp_15, marginBottom: Dimens.dp_20}}>{content}</Text>
                <Text style={{color: 'black', fontSize: Dimens.dp_40, fontWeight: 'bold'}}>0</Text>
                <Image source={icon} style={{width: Dimens.dp_30, height: Dimens.dp_30, resizeMode: 'contain',
                    position: 'absolute', bottom: Dimens.dp_15, right: Dimens.dp_15}}></Image>
            </View>
            </TouchableOpacity>
            </View>
        );
    }
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <Left>
                    <Button transparent onPress={() => {this._goToProfile()}}>
                    <Image source={menuIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}/>
                    </Button>
                </Left>
                <Body>
                    <Text style={{color: 'white', fontSize: Dimens.titleToolbar}}>Dashboard</Text>
                </Body>
                <Right />
                </Header>
                <Image source={dashboardBg} style={{flex: 1,
                    height: null, 
                    width: null, 
                    resizeMode: 'stretch' }}>
                    <ListView
                        dataSource= {this.state.dataSource}
                        renderRow={this._renderRow.bind(this)}
                        contentContainerStyle={{justifyContent: 'center', padding: 20, flexWrap: 'wrap', flexDirection: 'row'}}>
                    </ListView>
                </Image>
            </Container>
        );
    }

    _onClickItem(rowId) {
        switch(rowId) {
            case '0':
                Actions.notcheck();
                break;
            case '1':
                Actions.teacherofftoday();
                break;
            case '2':
                Actions.today();
                break;
            case '3':
                Actions.teacher();
                break;
        }
    }
    _goToProfile() {
        Actions.profile();
    }
}