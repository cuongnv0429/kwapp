import React, {Component} from 'react';
import {View, Text, Image, ListView, TouchableOpacity, Dimensions} from 'react-native';
import {Container, Content, Header, Left, Body, Right, Button   } from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const backIcon = require('../../../img/back.png');
const classIcon = require('../../../img/class.png');
const centralIcon = require('../../../img/central.png');
const locaiontIcon = require('../../../img/location.png');
const editReviewIcon = require('../../../img/edit_review.png');

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
var data = [];
const deviceWidth = Dimensions.get('window').width;
export default class ScheduleDetail extends Component {

    constructor(props) {
        super(props);
        for (var i = 0; i < 10; i++) {
            data.push(i);
        }
        this.state = {
            dataSource: ds.cloneWithRows(data),
        };
    }

    _renderRow(rowData, sectionID, rowID) {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', 
            width: deviceWidth, marginTop: 30}}>
            <TouchableOpacity onPress={ ()=> {this._onCLickItem(rowID)}}>
                <View style={{width: deviceWidth * 0.85 + Dimens.largelIcon, alignItems: 'center', justifyContent: 'center'}}>
                <View style={{ width: deviceWidth * 0.85, borderRadius: 15, padding: 10,
                    backgroundColor: '#e1e1e1', justifyContent: 'center'}}>
                    <View style={{flex: 0.3, flexDirection: 'row', padding: 5}}>
                        <View style={{flex: 0.5, flexDirection: 'row', alignItems: 'center'}}>
                            <Image source={classIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, marginRight: 10, 
                                resizeMode: 'contain'}}/>
                            <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>KTH3 - AE3</Text>
                        </View>
                        <View style={{flex: 0.5, flexDirection: 'row', marginLeft: 10, alignItems: 'center'}}>
                            <Text style={{fontSize: Dimens.dp_12, color: 'black'}}>09:30AM | 10:30AM</Text>
                        </View>
                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', padding: 5, alignItems: 'center'}}>
                        <Image source={centralIcon} style={{width: Dimens.smallIcon, height: Dimens.smallIcon,
                            marginRight: 10, resizeMode: 'contain'}}/>
                        <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>ENGLISH FOR AGES 4-6 YEARS</Text>
                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', padding: 5, alignItems: 'center'}}>
                        <Image source={locaiontIcon} style={{width: Dimens.smallIcon, height: Dimens.smallIcon,
                            marginRight: 10, resizeMode: 'contain'}}/>
                        <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>VATC </Text>
                    </View>
                </View>
                <View style={{ width: deviceWidth, position: 'absolute',
                     alignItems: 'flex-end', paddingRight: Dimens.largelIcon / 2}}>
                        <Image source={editReviewIcon} 
                            style={{width: Dimens.largelIcon, height: Dimens.largelIcon,
                            }}></Image>
                </View>
                </View>
                </TouchableOpacity>
            </View>
        )
    }
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 0.1}}>
                        <Button transparent onPress={() => {Actions.pop()}}>
                            <Image source={backIcon} style={{width: 20, height: 20}}/>
                        </Button>
                    </View>
                    <View style={{flex: 0.9, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'white', fontSize: 18}}>10-03-2017</Text>
                    </View>
                    <View style={{flex: 0.1}}>
                    </View>
                </View>
                </Header>
                <View style={{flex: 1, flexDirection: 'column'}}>
                    <ListView
                        style={{backgroundColor: 'transparent'}}
                        dataSource={this.state.dataSource}
                        renderRow={this._renderRow.bind(this)}/>
                    <View style={{height: 50, backgroundColor: 'transparent'}}></View>
                </View>
            </Container>
        );
    }

    _onCLickItem(rowId) {
        Actions.review({type: ActionConst.PUSH, rowID: rowId});
    }
}