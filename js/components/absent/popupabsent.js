import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View, Dimensions,StyleSheet, Image, PropTypes,
        ListView, ActivityIndicator} from 'react-native';
import {Button} from 'native-base';

const deviceHeight =  Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const Strings = require('../../../string');
var data = {},
    sectionIDs = [],
    rowIDs = [];
var getSectionData = (data, sectionId) => {
    return data[sectionId];
}
var getRowData = (data, sectionId, rowId) => {
    return data[sectionId + ':' + rowId];
}    
const ds = new ListView.DataSource({
        getSectionData: getSectionData,
        getRowData: getRowData,
        rowHasChanged: (row1, row2) => row1 !== row2,
        sectionHeaderHasChanged: (s1, s2) => s1 !== s2
    });
export class PopupAbsent extends Component {

  state = {

  }
  constructor (props) {
    super(props);
    if (sectionIDs.length < 1) {
        for (var i = 0; i < 10; i++) {
            sectionIDs.push(i);
            data[i] = 'cuong';
            rowIDs[i] = [];
            for (var j = 0; j < 2; j++) {
                rowIDs[i].push(j);
                data[i + ':' + j] = 'NVC';
            }
        }
    }
    this.state.visible = props.isVisible;
    this.state.dataSource = ds.cloneWithRowsAndSections(data, sectionIDs, rowIDs);
  }
  _setVisiblePopup(visible) {
      this.setState({
          visible: visible
      });
  }
  _callback(value) {
    this.props.oncallback(value);
    this.setState({
          visible: false
    });
  }
  renderSectionHeader(sectionData, sectionID) {
        return (
            <View style={{flex: 1, flexDirection: 'row', 
                paddingBottom: 15, paddingTop: 15, backgroundColor: 'white'}}>
                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center',}}>
                    <Text style={{color: 'black'}}>WEDNESDAY</Text>
                </View>
                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: 'black'}}>15-03-17</Text>
                </View>
            </View>
        ); 
    }
  renderRow (rowData, sectionID, rowID) {
    return (
            <View style={{marginLeft: 20, marginRight: 20,padding: 5, flex: 1, flexDirection: 'row', borderBottomWidth: 0.5,
                borderLeftWidth: 1, borderRightWidth: 1, borderTopWidth: 1, borderColor: '#D3D3D3'}}>
                <View style={{flex: 0.5}}>
                    <Text style={{color: 'black'}}>KHT3-AE3</Text>
                </View>
                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: 'black'}}>07:30AM | 09:30AM</Text>
                </View>
            </View> 
        );
  }
  render() {
    return (
            <View>
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={this.state.visible}>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)'}}>
                    <View style={{width: deviceWidth * 0.9, backgroundColor: 'white'}}>
                        <View style={{width: deviceWidth * 0.9, paddingTop: 10, paddingBottom: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#d3deff'}}>
                            <Text style={{fontSize: 15, color: 'black'}}>{Strings.textOffDays.toLocaleUpperCase()}</Text>
                        </View>
                        <View style={{height: deviceHeight * 0.5}}>
                            <ListView
                                dataSource = {this.state.dataSource}
                                renderRow  = {this.renderRow}
                                renderSectionHeader = {this.renderSectionHeader}/>
                        </View>
                        <View style={{height: deviceWidth * 0.2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                <View style={{flex: 0.5, flexDirection: 'row', width: deviceWidth * 0.3, alignItems: 'center', justifyContent: 'center'}}>
                                    <Button rounded onPress ={() => {this._callback('cancle')}}
                                    style={{backgroundColor: '#18285a', width: deviceWidth * 0.3,
                                    height: deviceWidth * 0.1,
                                    justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textCancel.toLocaleUpperCase()}</Text></Button>
                                </View>

                                <View style={{flex: 0.5, flexDirection: 'row', width: deviceWidth * 0.3, alignItems: 'center', justifyContent: 'center'}}>
                                    <Button rounded onPress ={() => {this._callback('ok')}}
                                    style={{backgroundColor: '#b51218', width: deviceWidth * 0.3,
                                    height: deviceWidth * 0.1,
                                    justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textOk.toLocaleUpperCase()}</Text></Button>
                                </View>
                        </View>
                    </View>
                </View>
            </Modal>
            </View>
        );
  }
}