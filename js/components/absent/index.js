import React, {Component} from 'react';
import {View, Text, Dimensions} from 'react-native';
import {Container, Content, Header, Button} from 'native-base';
import {Actions, ActionConst} from 'react-native-router-flux';
import {connect} from 'react-redux';
import Calendar from 'react-native-calendar';
import {PopupAbsent} from './popupabsent.js';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const deviceWidth = Dimensions.get('window').width;
const customStyle = {
    calendarHeading: {
      backgroundColor: '#020b62',
    },
    calendarContainer: {
      backgroundColor: 'white',
    },
    day: {
      color: '#BDBDBD',
    },
    dayHeading: {
        color: 'white'
    },
    weekendHeading: {
        color: 'white'
    },
    weekendDayText: {
      color: '#BDBDBD',
    },
    eventIndicator: {
        backgroundColor: '#cccccc',
        width: 6,
        height: 6,
        borderRadius: 3
    },
    hasEventText: {
      color: '#cccccc',
    },
    selectedDayCircle: {
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 0.1
    },
    selectedDayText: {
        color: 'black',
        fontWeight: 'normal',
    },
};
class Absent extends Component {
    render() {
        return (
            <Container>
                <PopupAbsent ref={(poppup) => { this.poppup = poppup }}
                    isVisible={false}
                    onRequestClose={() => {console.log('close')}}
                    oncallback={this._callback.bind(this)}/>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 0.1}}>
                    </View>
                    <View style={{flex: 0.9, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'white', fontSize: 18}}>{Strings.titleAbsent}</Text>
                    </View>
                    <View>
                        <Button transparent onPress={() => {alert(1)}}>
                            <Text style={{color: 'white', fontSize: Dimens.dp_12}}>{Strings.textReset}</Text>
                        </Button>
                    </View>
                </View>
                </Header>
                <Content style={{flex: 1, backgroundColor: 'white'}}>
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: Dimens.dp_20, marginBottom: Dimens.dp_10}}>
                        <Text style={{color: '#18285a', fontSize: Dimens.dp_18}}>{Strings.textSelectTheDay.toLocaleUpperCase()}</Text>
                    </View>
                    <Calendar 
                        scrollEnabled
                        dayHeadings = {['S', 'M', 'T', 'W', 'T', 'F', 'ST']}
                        onSwipeNext={(e) => console.log('onSwipeNext')}
                        showEventIndicators
                        events={
                            [
                                {date: '2017-04-09',  eventIndicator: {
                                backgroundColor: 'black',
                                width: 6,
                                height: 6,
                                borderRadius: 3,},hasEventText: {
                                color: 'black'}}
                            ]
                        }
                        eventDates={['2017-04-27', '2017-04-07']}
                        customStyle={customStyle} 
                        onDateSelect={(date) => this._onDateSelect(date)}/>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: Dimens.dp_30, marginBottom: Dimens.dp_30}}>
                        <Button rounded style={{backgroundColor: '#b51218', width: deviceWidth * 0.3,
                              height: deviceWidth * 0.1,
                              justifyContent: 'center', borderRadius: 10}}
                              onPress = {() => {this._onSubmit()}}>
                            <Text style={{color: 'white'}}>{Strings.textSubmit.toLocaleUpperCase()}</Text>
                        </Button>
                    </View>
                    <View style={{height: 50, backgroundColor: 'transparent'}}></View>
                </Content>
            </Container>
        );
    }

    _callback(value) {

    }
    _onDateSelect(date) {
        Actions.absentdetail({type: ActionConst.PUSH});
    }

    _onSubmit() {
        this.poppup._setVisiblePopup(true);
    }
}

function bindActions(distpatch) {
    return {

    };
};

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, bindActions)(Absent);