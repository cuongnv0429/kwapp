import React, { Component } from 'react';
import { Image, View, StatusBar, ListView, Dimensions, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { Container, Button, H3, Text, Header, Left, Body, Title, Right} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';

import StatisticsTeachedTime from './statisticsteachedtime.js';
import { openDrawer } from '../../actions/drawer';
import styles from './styles';

const menuIcon = require('../../../img/menu.png');
const dashboardBg = require('../../../img/bg_dashboard.png');
const classIcon = require('../../../img/class.png');
const centralIcon = require('../../../img/central.png');
const locaiontIcon = require('../../../img/location.png');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Strings = require('../../../string');
const Dimens = require('../../../dimen');

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
var data = [];
class DashBoard extends Component { // eslint-disable-line

  constructor(props) {
    super(props);
    for (var i = 0; i < 10; i++) {
        data.push(i);
    }
    this.state = {
      dataSource: ds.cloneWithRows(data),
    };
  }
  static propTypes = {
    openDrawer: React.PropTypes.func,
  }

  _renderRow(rowData, sectionID, rowID) {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', 
         width: deviceWidth, marginTop: Dimens.dp_30}}>
         <TouchableOpacity onPress={ ()=> {this._onClickItem(rowID)}}>
            <View style={{ width: deviceWidth * 0.9, borderRadius: 15, padding: 10,
                backgroundColor: rowID == 0 ? 'rgba(181, 18, 24, 0.5)' : 'rgba(255, 255, 255, 0.5)'}}>
                <View style={{flex: 0.3, flexDirection: 'row', padding: 5}}>
                    <View style={{flex: 0.5, flexDirection: 'row', alignItems: 'center'}}>
                        <Image source={classIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, marginRight: 10, 
                            resizeMode: 'contain', tintColor: rowID == 0 ? 'white' : 'black'}}/>
                        <Text style={{fontSize: Dimens.nomalText, color: rowID == 0 ? 'white' : 'black'}}>KTH3 - AE3</Text>
                    </View>
                    <View style={{flex: 0.5, flexDirection: 'column',
                         borderLeftWidth: 1, borderLeftColor: rowID == 0 ? 'white' : 'black', marginLeft: 10}}>
                        <Text style={{marginLeft: 5, fontSize: Dimens.dp_12, color: rowID == 0 ? 'white' : 'black'}}>TODAY</Text>
                        <Text style={{marginLeft: 5, fontSize: Dimens.dp_12, color: rowID == 0 ? 'white' : 'black'}}>09:30AM | 10:30AM</Text>
                    </View>
                </View>
                <View style={{flex: 0.3, flexDirection: 'row', padding: 5, alignItems: 'center'}}>
                    <Image source={centralIcon} style={{width: Dimens.smallIcon, height: Dimens.smallIcon,
                         marginRight: 10, resizeMode: 'contain', tintColor: rowID == 0 ? 'white' : 'black'}}/>
                    <Text style={{fontSize: Dimens.nomalText, color: rowID == 0 ? 'white' : 'black'}}>ENGLISH FOR AGES 4-6 YEARS</Text>
                </View>
                <View style={{flex: 0.3, flexDirection: 'row', padding: 5, alignItems: 'center'}}>
                    <Image source={locaiontIcon} style={{width: Dimens.smallIcon, height: Dimens.smallIcon,
                         marginRight: 10, resizeMode: 'contain', tintColor: rowID == 0 ? 'white' : 'black'}}/>
                    <Text style={{fontSize: Dimens.nomalText, color: rowID == 0 ? 'white' : 'black'}}>VATC </Text>
                </View>
            </View>
            </TouchableOpacity>
        </View>
    )
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
          <Left>
            <Button transparent onPress={() => {this._goToProfile()}}>
              <Image source={menuIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}/>
            </Button>
          </Left>
          <Body>
            <Text style={{color: 'white', fontSize: Dimens.titleToolbar}}>MARCH 10TH</Text>
          </Body>
          <Right />
        </Header>
          <Image source={dashboardBg} style={{flex: 1,
            height: null, 
            width: null, 
            resizeMode: 'stretch' }}>
        <View style={{flex: 0.2}}>
            <StatisticsTeachedTime/>
        </View>
        <View style={{flex: 0.8, flexDirection: 'column'}}>
            <ListView
                style={{backgroundColor: 'transparent'}}
                dataSource={this.state.dataSource}
                renderRow={this._renderRow.bind(this)}></ListView>
            <View style={{height: 50, backgroundColor: 'transparent'}}></View>
        </View>
        </Image>
      </Container>
    );
  }

  _goToProfile() {
      Actions.profile();
  }

  _onClickItem(rowId) {
    Actions.checkin({rowId: rowId});
  }
}

function bindActions(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
  themeState: state.drawer.themeState,
  routes: state.drawer.routes,
});

export default connect(mapStateToProps, bindActions)(DashBoard);
