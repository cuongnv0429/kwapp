import React, {Component} from 'react';
import {View, Text, Image, Dimensions} from 'react-native';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const Colors = require('../../../color');
const widthScreen = Dimensions.get('window').width;

const bookIcon = require('../../../img/book.png');
const offDayIcon = require('../../../img/off_day.png');
const otDayIcon = require('../../../img/ot_day.png');


export default class StatisticsTeachedTime extends Component{
    render() {
        return (
            <View style={{flex: 1, backgroundColor: Colors.colorWhite, width: widthScreen,
             flexDirection: 'column', alignItems: 'center', justifyContent: 'center', 
             height: null}}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: Colors.colorBlack, fontSize: Dimens.dp_30}}>30</Text>
                    </View>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: Colors.colorBlack, fontSize: Dimens.dp_30}}>01</Text>
                    </View>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: Colors.colorBlack, fontSize: Dimens.dp_30}}>00</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: Colors.colorBlack, marginRight: 5}}>{Strings.taughtHours.toLocaleUpperCase()}</Text>
                        <Image source={bookIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}/>
                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: Colors.colorBlack, marginRight: 5}}>{Strings.daysOff.toLocaleUpperCase()}</Text>
                        <Image source={offDayIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}/>
                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: Colors.colorBlack, marginRight: 5}}>{Strings.overTime.toLocaleUpperCase()}</Text>
                        <Image source={otDayIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}/>
                    </View>
                </View>
            </View>
        )
    };
}