import React, {Component} from 'react';
import {View, Text, Image, Dimensions} from 'react-native';
import {Container,Content, Button, Header} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';

const backIcon = require('../../../img/back.png');
const Dimens = require('../../../dimen');
const Strings = require('../../../string');

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class NotCheckReview extends Component {
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 0.1, alignItems: 'center'}}>
                            <Button transparent onPress={() => {Actions.pop()}}>
                                <Image source={backIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}/>
                            </Button>
                        </View>
                        <View style={{flex: 0.8, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: Dimens.titleToolbar}}>
                                Nguyen Van B
                            </Text>
                        </View>
                        <View style={{flex: 0.1}}/>
                    </View>
                </Header>
                <Content>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, alignItems: 'center', marginTop: Dimens.dp_15, marginBottom: Dimens.dp_15}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_20}}>KTH3 - AE3</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center', marginBottom: Dimens.dp_15}}>
                        <Text style={{color: 'black', fontSize: Dimens.nomalText}}>07:30AM - 09:30AM</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center', marginBottom: Dimens.dp_15}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_20}}>TIME TO KNOW</Text>
                    </View>
                    <View style={{flexDirection: 'row', marginLeft: Dimens.dp_20, marginRight: Dimens.dp_20, marginBottom: Dimens.dp_20}}>
                        <View style={{flex: 0.35, backgroundColor: '#b51218', padding: Dimens.dp_5, marginRight: Dimens.dp_15, justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: Dimens.nomalText}}>IN 07:20AM</Text>
                        </View>
                        <View style={{flex: 0.65}}>
                            <Text style={{color: 'black', fontSize: Dimens.nomalText}} numberOfLines={2} ellipsizeMode='tail' lineBreakMode='tail'>
                                42/14 Nguyen Huu Tien street, Tay Thanh ward, Tan Phu district, HCMC
                            </Text>
                        </View>
                        
                    </View>
                    <View style={{flexDirection: 'row', marginLeft: Dimens.dp_20, marginRight: Dimens.dp_20, marginBottom: Dimens.dp_20}}>
                        <View style={{flex: 0.35, backgroundColor: '#b51218', padding: Dimens.dp_5, marginRight: Dimens.dp_15, justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: Dimens.nomalText}}>OUT 09:40 AM</Text>
                        </View>
                        <View style={{flex: 0.65}}>
                            <Text style={{color: 'black', fontSize: Dimens.nomalText}} numberOfLines={2} ellipsizeMode='tail' lineBreakMode='tail'>
                                42/14 Nguyen Huu Tien street, Tay Thanh ward, Tan Phu district, HCMC
                            </Text>
                        </View>
                    </View>
                    <View style={{backgroundColor: '#F5F5F5', marginLeft: Dimens.dp_20, marginRight: Dimens.dp_20, padding: Dimens.dp_15, height: deviceHeight * 0.3, alignItems: 'flex-start'}}>
                        <AutoGrowingTextInput autoFocus = {true} maxHeight = {deviceHeight * 0.4} 
                            placeholder={Strings.hintNoteSomeThings} underlineColorAndroid ='transparent'/>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'row', marginTop: Dimens.dp_30,
                                 alignItems: 'center', justifyContent: 'center', marginBottom: deviceWidth / 5}}>
                            <Button rounded onPress ={() => {alert(1)}}
                            style={{backgroundColor: '#b51218', width: deviceWidth / 2,
                            height: deviceWidth / 6,
                            justifyContent: 'center', borderRadius: 20}}><Text style={{color: 'white'}}>{Strings.textSubmit.toLocaleUpperCase()}</Text></Button>
                    </View>
                </View>
                </Content>
            </Container>
        );
    }
}