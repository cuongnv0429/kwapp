import React, {Component} from 'react';
import {View, Text, Image, ListView, Dimensions} from 'react-native';
import {Container, Content, Header, Left, Button, Body, Right} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {PopupWarning} from './popupwarning.js';
import {connect} from 'react-redux';
const backIcon = require('../../../img/back.png');
const checkIcon = require('../../../img/check.png');
const notCheckIcon = require('../../../img/not_check.png');
const Strings = require('../../../string');
const Dimens = require('../../../dimen');


const deviceWidth = Dimensions.get('window').width;
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
var data = [];
class Attendance extends Component {    
    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }
        this.state = {
            datasource: ds.cloneWithRows(data)
        }
    }

    _renderRow(rowData, sectionId, rowId) {
        var backgroundView = 'white';
        if (rowId % 2 == 1) {
            backgroundView = '#efefef';
        }
        return(
            <View style={{flex: 1, flexDirection: 'row', marginLeft: 10, marginRight: 10, backgroundColor: backgroundView}}>
                <View style={{flex: 0.8, justifyContent: 'center', marginLeft: 10}}>
                    <Text style={{fontSize: Dimens.dp_15, color: 'black'}}>Nguyen Van Cuong</Text>
                </View>
                <View style={{flex: 0.2,paddingTop: 5, paddingBottom: 5, alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
                    <Image source={notCheckIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}></Image>
                    <Text style={{fontSize: Dimens.dp_12, color: 'black', marginTop: 5}}>09:00</Text>
                </View>
            </View>
        );
    }

    render() {
        return (
            <Container>
                <PopupWarning ref={(poppup) => { this.poppup = poppup }}
                    isVisible={false}
                    onRequestClose={() => {console.log('close')}}
                    oncallback={this._callback.bind(this)}/>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <Left>
                    <Button transparent onPress={() => {Actions.pop()}}>
                        <Image source={backIcon} style={{width: 20, height: 20}}/>
                    </Button>
                </Left>
                <Body>
                    <Text style={{color: 'white', fontSize: 18}}>{Strings.titleAttendance}</Text>
                </Body>
                <Right/>
                </Header>
                <View style={{flex: 1, flexDirection: 'column'}}>
                    <View style={{flexDirection: 'row', backgroundColor: '#d3deff', margin: 10, padding: Dimens.dp_15}}>
                        <View style={{flex: 0.5, flexDirection: 'column', alignItems: 'center', justifyContent: 'center', borderRightWidth: 0.5, borderRightColor: 'white'}}>
                            <Text style={{color: 'black', fontSize: Dimens.dp_20, fontWeight: 'bold', marginRight: 10, marginBottom: 10}}>KTH3 - AE3</Text>
                            <Text style={{color: 'black', fontSize: Dimens.dp_12, marginRight: 10}}>09:30AM | 10:30AM</Text>
                        </View>
                        <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'black', fontSize: Dimens.dp_20, marginLeft: 10}}>ENGLISH FOR AGES 4-6 YEARS</Text>
                        </View>
                    </View>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <ListView
                            dataSource={this.state.datasource}
                            renderRow={this._renderRow.bind(this)}/>
                        <View style={{flexDirection: 'row', marginTop: 20,
                            alignItems: 'center', justifyContent: 'center', marginBottom: deviceWidth / 5}}>
                            <Button rounded onPress ={() => {this._checkOut()}}
                                style={{backgroundColor: '#b51218', width: deviceWidth / 2,
                                height: deviceWidth / 6,
                                justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.checkOut.toLocaleUpperCase()}</Text></Button>
                    </View>
                    </View>
                    
                </View>
                
            </Container>
        );
    }

    _checkOut() {
        this.poppup._setVisiblePopup(true);
    }

    _callback(value) {
        console.log(value + ">>attendance");
        switch(value) {
            case 'ok':
                Actions.checkout();
                break;
            case 'cancle':
                break;
        }
    }
}

function bindActions(dispatch) {
    return({

    });
}

const mapStateToProp = state => ({

});

export default connect(mapStateToProp, bindActions) (Attendance);