import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View, Dimensions,StyleSheet, Image, PropTypes,
        ListView, ActivityIndicator} from 'react-native';
import {Button} from 'native-base';

const deviceHeight =  Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const Strings = require('../../../string');
export class PopupWarning extends Component {

  state = {

  }
  constructor (props) {
    super(props);
    console.log(props);
    this.state.visible = props.isVisible;
  }
  _setVisiblePopup(visible) {
      console.log(visible);
      this.setState({
          visible: visible
      });
  }
  _callback(value) {
    this.props.oncallback(value);
    this.setState({
          visible: false
    });
  }
  render() {
    return (
            <View>
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={this.state.visible}>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)'}}>
                <View style={{width: deviceWidth * 0.8, backgroundColor: 'white', paddingBottom: 20,
                  alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
                  <View style={{width: deviceWidth * 0.8, paddingTop: 10, paddingBottom: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#d3deff'}}>
                        <Text style={{fontSize: 15, color: 'black'}}>{Strings.textWarning.toLocaleUpperCase()}</Text>
                  </View>
                  <View style={{width: deviceWidth * 0.7, marginTop: 20, marginBottom: 20, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: 12, color: 'black'}}>{Strings.messageAlert.toLocaleUpperCase()}</Text>
                  </View>
                  <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 20,marginBottom: 20}}>
                        <View style={{flex: 0.5, flexDirection: 'row', width: deviceWidth * 0.3, alignItems: 'center', justifyContent: 'center'}}>
                              <Button rounded onPress ={() => {this._callback('cancle')}}
                              style={{backgroundColor: '#18285a', width: deviceWidth * 0.3,
                              height: deviceWidth * 0.1,
                              justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textCancel.toLocaleUpperCase()}</Text></Button>
                        </View>

                        <View style={{flex: 0.5, flexDirection: 'row', width: deviceWidth * 0.3, alignItems: 'center', justifyContent: 'center'}}>
                              <Button rounded onPress ={() => {this._callback('ok')}}
                              style={{backgroundColor: '#b51218', width: deviceWidth * 0.3,
                              height: deviceWidth * 0.1,
                              justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textOk.toLocaleUpperCase()}</Text></Button>
                        </View>
                  </View>
                  
                </View>
                </View>
            </Modal>
            </View>
        );
  }
}