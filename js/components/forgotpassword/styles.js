const React = require('react-native');
const {StyleSheet, Dimensions} = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
    imageBg: {
        width: null,
        height: null,
        flex: 1,
    }
}