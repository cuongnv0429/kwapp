import React, {Component} from 'react';
import {View, Text, Image, TextInput, Dimensions} from 'react-native';
import {Container, Content, Header, Left, Button, Body, Right, Title} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';
const imageBG = require('../../../img/bg_login.png');
const backIcon = require('../../../img/back.png');
const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const styles = require('./styles.js');

const deviceWith = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

export default class ForgotPassword extends Component {
    render() {
        return (
            <Container>
                <View style={{flex: 1}}>
                    <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <Left>
                        <Button transparent onPress={() => {Actions.pop()}}>
                            <Image source={backIcon} style={{width: 20, height: 20}}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{color: 'white', fontSize: 18, width: deviceWith * 0.6}}>{Strings.titleFogotPassword}</Title>
                    </Body>
                    <Right />
                    </Header>
                    <Image source={imageBG} style={{width : deviceWith, height: deviceHeight, resizeMode: 'cover'}}>
                    <Content>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <View style={{width: deviceWith * 0.8, marginTop: deviceWith * 0.4, borderBottomWidth: 0.5, borderBottomColor: '#6e799d'}}>
                                <TextInput placeholder={Strings.hintEmail} placeholderTextColor = 'black'
                                    underlineColorAndroid ='transparent'
                                    style={{width: deviceWith * 0.8, height: 40}}></TextInput>
                            </View>
                            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: Dimens.dp_30}}>
                                <Button rounded onPress ={() => {alert(1)}}
                                style={{backgroundColor: '#b51218', width: deviceWith / 2,
                                justifyContent: 'center', borderRadius: 10, height: 30}}><Text style={{color: 'white'}}>{Strings.textsend}</Text></Button>
                            </View>
                        </View>
                        
                    </Content>
                    </Image>
                </View>
            </Container>
        );
    }
}