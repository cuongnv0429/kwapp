import React, {Component} from 'react';
import {View, Image, Text, Dimensions, TouchableOpacity} from 'react-native';
import {Container, Content, Button, Header, Left, Right, Body} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';

const backIcon = require('../../../img/back.png');
const classIcon = require('../../../img/class.png');
const centralIcon = require('../../../img/central.png');
const locaiontIcon = require('../../../img/location.png');
const Dimens = require('../../../dimen');
const Strings = require('../../../string');
const deviceWidth = Dimensions.get('window').width;

export default class TeacherOffTodayDetail extends Component {
    render () {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 0.1, alignItems: 'center'}}>
                            <Button transparent onPress={() => {Actions.pop()}}>
                                <Image source={backIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}/>
                            </Button>
                        </View>
                        <View style={{flex: 0.8, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: Dimens.titleToolbar}}>
                                Nguyen Van Z
                            </Text>
                        </View>
                        <View style={{flex: 0.1}}/>
                    </View>
                </Header>
                <Content>
                    <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                        <Text style={{color: '#2C3767', fontSize: Dimens.nomalText, marginBottom: Dimens.dp_20, marginTop: Dimens.dp_20}}>
                            THE DAY WANT TO CHANGE
                        </Text>
                        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1, width: deviceWidth * 0.8, 
                            marginLeft: Dimens.dp_20, marginRight: Dimens.dp_20, borderColor: '#DDDDDD', 
                            borderWidth: 0.5, marginBottom: Dimens.dp_20, paddingBottom: Dimens.dp_10
                            , paddingTop: Dimens.dp_10}}>
                            <Text style={{color: '#2C3767', fontSize: Dimens.dp_18}}>
                                10-03-2017
                            </Text>
                        </View>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', width: deviceWidth}}>
                            <View style={{ width: deviceWidth * 0.9, borderRadius: 15, padding: 10,
                                backgroundColor: '#DADADA'}}>
                                <View style={{flex: 0.3, flexDirection: 'row', padding: 5}}>
                                    <View style={{flex: 0.5, flexDirection: 'row', alignItems: 'center'}}>
                                        <Image source={classIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, marginRight: 10, 
                                            resizeMode: 'contain'}}/>
                                        <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>KTH3 - AE3</Text>
                                    </View>
                                    <View style={{flex: 0.5, flexDirection: 'column', marginLeft: 10}}>
                                        <Text style={{marginLeft: 5, fontSize: Dimens.dp_12, color: 'black'}}>09:30AM | 10:30AM</Text>
                                    </View>
                                </View>
                                <View style={{flex: 0.3, flexDirection: 'row', padding: 5, alignItems: 'center'}}>
                                    <Image source={centralIcon} style={{width: Dimens.smallIcon, height: Dimens.smallIcon,
                                        marginRight: 10, resizeMode: 'contain'}}/>
                                    <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>ENGLISH FOR AGES 4-6 YEARS</Text>
                                </View>
                                <View style={{flex: 0.3, flexDirection: 'row', padding: 5, alignItems: 'center'}}>
                                    <Image source={locaiontIcon} style={{width: Dimens.smallIcon, height: Dimens.smallIcon,
                                        marginRight: 10, resizeMode: 'contain'}}/>
                                    <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>VATC </Text>
                                </View>
                            </View>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', marginTop: 30,
                                 alignItems: 'center', justifyContent: 'center', marginBottom: deviceWidth / 5}}>
                            <Button rounded onPress ={() => {alert(1)}}
                            style={{backgroundColor: '#b51218', width: deviceWidth / 2,
                            height: deviceWidth / 6,
                            justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textSubmit.toLocaleUpperCase()}</Text></Button>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}