import React, {Component} from 'react';
import {View, Text, Image, ListView, TouchableOpacity} from 'react-native';
import {Container, Content, Header, Left, Button, Body, Right} from 'native-base';
import {Actions, ActionConst} from 'react-native-router-flux';
const backIcon = require('../../../img/back.png');
const payrollIcon = require('../../../img/payroll.png');
const Strings = require('../../../string');
const Dimens = require('../../../dimen');
import Calendar from 'react-native-calendar';
import TimeTable from '../timetable';
import ScheduleByMonth from './schedulebymonth';

export default class Schedule extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{flex: 0.1}}>
                    </View>
                    <View style={{flex: 0.9, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'white', fontSize: 18}}>{Strings.titleCalendar.toLocaleUpperCase()}</Text>
                    </View>
                    <View style={{flex: 0.1}}>
                        <Button transparent onPress={() => {this._goToPayroll()}}>
                            <Image source={payrollIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}></Image>
                        </Button>
                    </View>
                </View>
                </Header>
                <Content style={{flex: 1, backgroundColor: 'white'}}>
                    <TimeTable
                        onDateSelect={this._onDateSelect.bind(this)}/>
                    <ScheduleByMonth/>
                    <View style={{height: 50, backgroundColor: 'transparent'}}></View>
                </Content>
            </Container>
        );
    }

    _onDateSelect(date) {
        Actions.scheduledetail({type: ActionConst.PUSH});
    }

    _goToPayroll() {
        Actions.payroll({type: ActionConst.PUSH});
    }

    
}