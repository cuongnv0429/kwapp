import React, {Component} from 'react';
import {View, Text, ListView, TouchableOpacity, Image} from 'react-native';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const Colors = require('../../../color');
const locationIcon = require('../../../img/location.png');

var data = [];
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});

export default class ScheduleByMonth extends Component {

    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }
        this.state = {
            dataSource: ds.cloneWithRows(data)
        }
    }

    _renderHeader() {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', 
                flexDirection: 'column', backgroundColor: '#f9f7f8', margin: Dimens.dp_10,
                paddingTop: Dimens.dp_15, paddingBottom: Dimens.dp_15}}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: Colors.colorBlack, fontSize: Dimens.dp_30}}>30</Text>
                    </View>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: Colors.colorBlack, fontSize: Dimens.dp_30}}>01</Text>
                    </View>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: Colors.colorBlack, fontSize: Dimens.dp_30}}>00</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: Colors.colorBlack}}>Hours teached</Text>

                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: Colors.colorBlack}}>{Strings.daysOff}</Text>
                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: Colors.colorBlack}}>{Strings.overTime}</Text>
                    </View>
                </View>
            </View>
        );
    }

    _renderRow(rowData, sectionId, rowId) {
        return(
            <TouchableOpacity>
                <View style={{flex: 1, flexDirection: 'row', backgroundColor: 'white',
                    paddingTop: Dimens.dp_15, paddingBottom: Dimens.dp_15, alignItems: 'center',
                    margin: Dimens.dp_10, borderColor: Colors.colorBorder, borderWidth: 0.5}}>
                    <Image source={locationIcon} style={{resizeMode: 'contain', width: Dimens.smallIcon,
                        height: Dimens.smallIcon, marginLeft: Dimens.dp_5, marginRight: Dimens.dp_5}}></Image>
                    <View style={{flex: 0.7, flexDirection: 'column'}}>
                        <Text style={{fontSize: Dimens.dp_12, color: Colors.colorBlack}}>VATC Trung Son</Text>
                        <Text style={{fontSize: Dimens.dp_15, color: Colors.colorBlack}}>ENGLISH FOR AGES 4-6 YEARS ENGLISH FOR AGES 4-6 YEARS</Text>
                    </View>
                    <View style={{flex: 0.2, flexDirection: 'row', marginLeft: 5}}>
                        <Text style={{fontSize: Dimens.dp_20, color: Colors.colorBlack}}>16
                            <Text style={{paddingLeft: 5, fontSize: Dimens.dp_10, color: Colors.colorBlack}}>hours</Text>
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View>
                <ListView
                    dataSource= {this.state.dataSource}
                    renderRow= {this._renderRow.bind(this)}
                    renderHeader= {this._renderHeader.bind(this)}/>
            </View>
        );
    }
}