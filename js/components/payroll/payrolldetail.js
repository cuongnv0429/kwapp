import React, {Component} from 'react';
import {View, Modal, Text, ListView, TouchableOpacity, Image, Dimensions, Platform} from 'react-native';

const locationIcon = require('../../../img/location.png');
const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const Colors = require('../../../color');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

var data = [];
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});

export default class PayrollDetail extends Component {

    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }
        this.state = {
            dataSource: ds.cloneWithRows(data), 
            modalVisible: false
        }
    }

    _renderHeader() {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', 
                flexDirection: 'column', backgroundColor: '#f9f7f8', margin: Dimens.dp_10,
                paddingTop: Dimens.dp_15, paddingBottom: Dimens.dp_15}}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText,textAlign: 'center', color: 'black'}}>Hours teached</Text>

                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>{Strings.daysOff}</Text>
                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>{Strings.overTime}</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_30}}>30</Text>
                    </View>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_30}}>01</Text>
                    </View>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_30}}>00</Text>
                    </View>
                </View>
                
            </View>
        );
    }

    _renderRowPopup(rowData, sectionId, rowId) {
        return(
            <TouchableOpacity>
                <View style={{flex: 1, flexDirection: 'row', backgroundColor: 'white',
                    paddingTop: Dimens.dp_15, paddingBottom: Dimens.dp_15, alignItems: 'center',
                    margin: Dimens.dp_10, borderColor: Colors.colorBorder, borderWidth: 0.5}}>
                    <Image source={locationIcon} style={{resizeMode: 'contain', width: Dimens.smallIcon,
                        height: Dimens.smallIcon, marginLeft: Dimens.dp_5, marginRight: Dimens.dp_5}}></Image>
                    <View style={{flex: 0.7, flexDirection: 'column'}}>
                        <Text style={{fontSize: Dimens.dp_10, color: 'black'}}>VATC Trung Son</Text>
                        <Text style={{fontSize: Dimens.dp_12, color: 'black'}}>ENGLISH FOR AGES 4-6 YEARS ENGLISH FOR AGES 4-6 YEARS</Text>
                    </View>
                    <View style={{flex: 0.2, flexDirection: 'row'}}>
                        <Text style={{fontSize: Dimens.dp_20, color: 'black'}}>16</Text>
                        <View style={{height: Dimens.dp_20, justifyContent: 'flex-end', flexDirection: 'column'}}>
                            <Text style={{fontSize: Dimens.dp_8, color: 'black', marginLeft: Dimens.dp_5}}>hours</Text>
                        </View>
                        
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    animationType={"fade"}
                    closeOnClick={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {Platform.OS === 'android' ? this.setState({modalVisible: false}) : ''}}>
                    <TouchableOpacity activeOpacity= {1} style={{flex: 1}} 
                        onPress = {()=> {this._onPressOutsite()}}>
                    <View style={{flex: 1,  alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)'}}>
                        <View style={{width: deviceWidth * 0.8, height: deviceHeight * 0.7, backgroundColor: 'white'}}>
                            <ListView
                                dataSource = {this.state.dataSource}
                                renderRow={this._renderRowPopup.bind(this)}
                                renderHeader={this._renderHeader.bind(this)}/>
                        </View>
                        
                    </View>
                    </TouchableOpacity>
                </Modal>
                 
            </View>
        );
    }

    _onPressOutsite() {
        console.log("dsds");
        this.setModalVisible(false);
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }
}