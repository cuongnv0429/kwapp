import React, {Component} from 'react';
import {View, Text, Image, ListView, Dimensions, TouchableOpacity} from 'react-native';
import {Container, Content, Header, Left, Body, Right, Button, Footer} from 'native-base';
import {Actions, ActionConst} from 'react-native-router-flux';
import PayrollDetail from './payrolldetail';

const backIcon = require('../../../img/back.png');
const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

var data = [];
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2});
export default class Payroll extends Component {

    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }

        this.state = {
            dataSource: ds.cloneWithRows(data), 
            dataSourcePP: ds.cloneWithRows(data), 
        }
    }

    _renderRow(rowData, sectionId, rowId) {
        return(
            <TouchableOpacity 
                onPress={() => {this.payrolldetaill.setModalVisible(true)}}>
            <View style={{flex: 1, flexDirection: 'column', padding: Dimens.dp_10, backgroundColor: '#e1e1e1', marginTop: Dimens.dp_20,
                marginLeft: Dimens.dp_10, marginRight: Dimens.dp_10}}>
                <Text style={{color: 'black', fontSize: Dimens.dp_15, marginBottom: Dimens.dp_10}}>NOVEMBER - 2016</Text>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>{Strings.textTaughtHours}</Text>

                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>{Strings.daysOff}</Text>
                    </View>
                    <View style={{flex: 0.3, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{fontSize: Dimens.nomalText, color: 'black'}}>{Strings.overTime}</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'column', alignItems: 'center', justifyContent: 'center', flex: 1}}>
                    <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_30}}>30</Text>
                    </View>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_30}}>01</Text>
                    </View>
                    <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{color: 'black', fontSize: Dimens.dp_30}}>00</Text>
                    </View>
                </View>
                </View>
            </View>
            </TouchableOpacity>
        );
    }
    
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                <Left>
                    <Button transparent onPress={() => {Actions.pop()}}>
                        <Image source={backIcon} style={{width: 20, height: 20}}/>
                    </Button>
                </Left>
                <Body>
                    <Text style={{color: 'white', fontSize: 18}}>{Strings.titlePayroll.toLocaleUpperCase()}</Text>
                </Body>
                <Right/>
                </Header>
                <View style={{flex: 1, flexDirection: 'column'}}>
                    <ListView
                        dataSource = {this.state.dataSource}
                        renderRow={this._renderRow.bind(this)}/>
                    <View style={{height: 50, backgroundColor: 'transparent'}}></View>
                </View>
                <PayrollDetail
                    ref={(payrolldetaill) => { this.payrolldetaill = payrolldetaill; }}/>
                {/*<PopupDialog
                    height={deviceHeight * 0.6}
                    width={deviceWidth * 0.9}
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}>
                    <View style={{flex: 1}}>
                        <ListView
                            dataSource = {this.state.dataSourcePP}
                            renderRow={this._renderRowPopup.bind(this)}
                            renderHeader={this._renderHeader.bind(this)}/>
                    </View>
                </PopupDialog>*/}
            </Container>
        );
    }
}