import React, {Component} from 'react';
import {View, Text, Image, ListView} from 'react-native';
import {Container, Content, Header, Button} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const avatar = require('../../../img/avatar_profile.png');
const backIcon = require('../../../img/back.png');

var data = [];
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 != r2})
export default class Teachers extends Component {

    constructor(props) {
        super(props);
        if (data.length < 1) {
            for (var i = 0; i < 10; i++) {
                data.push(i);
            }
        }

        this.state = ({
            dataSource: ds.cloneWithRows(data)
        });

    }
    _renderRow(rowData, sectiondId, rowId) {
        return (
            <View style={{flex: 1, flexDirection: 'row', paddingBottom: Dimens.dp_10, paddingTop: Dimens.dp_10, 
                borderBottomWidth: 0.5, borderBottomColor: '#DDDDDD'}}>
                <View style={{flex: 0.3, alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={avatar} style={{width: Dimens.dp_70, height: Dimens.dp_70, borderRadius: 35}}/>
                </View>
                <View style={{flex: 0.8, marginLeft: Dimens.dp_10}}>
                    <Text style={{color: 'black', fontSize: Dimens.dp_18}}>NGUYEN VAN X</Text>
                    <Text style={{color: 'black', fontSize: Dimens.nomalText}}>0962045878</Text>
                    <Text style={{color: 'black', fontSize: Dimens.nomalText}}>17 Ho Ba Kien P15 Q10 TP HCM</Text>
                </View>
            </View>
        );
    }
    render() {
        return (
            <Container>
                <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 0.1, alignItems: 'center'}}>
                            <Button transparent onPress={() => {Actions.pop()}}>
                                <Image source={backIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon}}/>
                            </Button>
                        </View>
                        <View style={{flex: 0.8, alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{color: 'white', fontSize: Dimens.titleToolbar}}>
                                {Strings.titleListTeachers}
                            </Text>
                        </View>
                        <View style={{flex: 0.1}}/>
                    </View>
                </Header>
                <View style={{flex: 1}}>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this._renderRow}>
                    </ListView>
                </View>
            </Container>
        );
    }
}