import React, {Component} from 'react';
import {View, Text} from 'react-native';

const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const Styles = require('./styles.js');
const Colors = require('../../../color');
import Calendar from 'react-native-calendar';

var customStyle = {
    calendarHeading: {
      backgroundColor: '#020b62',
    },
    calendarContainer: {
      backgroundColor: 'white',
    },
    day: {
      color: '#BDBDBD',
    },
    dayHeading: {
        color: 'white'
    },
    weekendHeading: {
        color: 'white'
    },
    weekendDayText: {
      color: '#BDBDBD',
    },
    eventIndicator: {
        backgroundColor: '#cccccc',
        width: 6,
        height: 6,
        borderRadius: 3
    },
    hasEventText: {
      color: '#cccccc',
    },
    selectedDayCircle: {
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 0.1
    },
    selectedDayText: {
        color: 'black',
        fontWeight: 'normal',
    },
}

export default class TimeTable extends Component{
    constructor(props) {
        super(props);
        this.state = {
            customStyle : customStyle
        }
    }
    render() {
        return(
            <View style={{backgroundColor: Colors.colorWhite}}>
                <Calendar 
                        scrollEnabled
                        dayHeadings = {['S', 'M', 'T', 'W', 'T', 'F', 'ST']}
                        onSwipeNext={(e) => console.log(e)}
                        showEventIndicators
                        events={
                            [
                                {date: '2017-04-09',  eventIndicator: {
                                backgroundColor: 'black',
                                width: 6,
                                height: 6,
                                borderRadius: 3,},hasEventText: {
                                color: 'black'}}
                            ]
                        }
                        eventDates={['2017-04-27', '2017-04-07']}
                        customStyle={this.state.customStyle} 
                        onDateSelect={(date) => this._onDateSelect(date)}/>
            </View>
        )
    }

    _onDateSelect(date) {
        this.props.onDateSelect(date);
    }

    setStyle(style) {
        this.setState({
            customStyle: style
        })
    }
}