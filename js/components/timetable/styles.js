
const React = require('react-native');

const { StyleSheet } = React;

export default {
    calendarHeading: {
      backgroundColor: '#020b62',
    },
    calendarContainer: {
      backgroundColor: 'white',
    },
    day: {
      color: '#BDBDBD',
    },
    dayHeading: {
        color: 'white'
    },
    weekendHeading: {
        color: 'white'
    },
    weekendDayText: {
      color: '#BDBDBD',
    },
    eventIndicator: {
        backgroundColor: '#cccccc',
        width: 6,
        height: 6,
        borderRadius: 3
    },
    hasEventText: {
      color: '#cccccc',
    },
    selectedDayCircle: {
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 0.1
    },
    selectedDayText: {
        color: 'black',
        fontWeight: 'normal',
    },
};
