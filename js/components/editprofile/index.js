import React, {Component} from 'react';
import {View, Text, Image, TextInput, Dimensions, TouchableOpacity, Platform} from 'react-native';
import {Container, Content, Header, Left, Button, Body, Right, Title} from 'native-base';
import { Actions, ActionConst} from 'react-native-router-flux';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import DateTimePicker from 'react-native-modal-datetime-picker';

const imageBG = require('../../../img/bg_login.png');
const backIcon = require('../../../img/back.png');
const cancleIcon = require('../../../img/cancle.png');
const Strings = require('../../../string');
const Dimens = require('../../../dimen');
const styles = require('./styles.js');
const mailIcon = require('../../../img/mail.png');
const locationIcon = require('../../../img/location.png');
const birthdayIcon = require('../../../img/birthday.png');
const phoneProfileIcon = require('../../../img/phone_profile.png');

const deviceWith = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

export default class EditProfile extends Component {

    constructor(props) {
        super(props);
        this.state = ({
            isDateTimePickerVisible: false
        });
    }
    render() {
        return (
            <Container>
                <View style={{flex: 1}}>
                    <Header style={{backgroundColor: '#18285a', alignItems: 'center', justifyContent: 'center'}}>
                    <Left>
                        <Button transparent onPress={() => {Actions.pop()}}>
                            <Image source={backIcon} style={{width: 20, height: 20}}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{color: 'white', fontSize: 18, width: deviceWith * 0.6}}>{Strings.titleEditProfile}</Title>
                    </Body>
                    <Right />
                    </Header>
                    <Content>
                        <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this._handleDatePicked.bind(this)}
                                onCancel={this._hideDateTimePicker.bind(this)}
                        />
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 10, 
                                width: deviceWith * 0.85, backgroundColor: '#EBEBEB', padding: Dimens.dp_10}}>
                                <Text style={{flex: 1, color: 'black', fontSize: Dimens.dp_12}}>Re-enter new password wrong</Text>
                                <Image source={cancleIcon} style={{width: Dimens.dp_20, height: Dimens.dp_20, marginLeft: 10}}></Image>
                            </View>
                            <View style={{width: deviceWith * 0.85, marginTop: Dimens.dp_20, borderBottomWidth: 0.5, 
                                borderBottomColor: '#6e799d'}}>
                                <TouchableOpacity onPress = {() => {this._showDateTimePicker()}}>
                                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                                    <Image source={birthdayIcon} style={{width: Dimens.nomalIcon, height: Dimens.nomalIcon, resizeMode: 'contain'}}/>
                                    <View style={{height: Dimens.dp_25, alignItems: 'center', justifyContent: 'center', marginLeft: Dimens.dp_10}}>
                                        <Text style={{color: 'black', fontSize: 14}}>mm-dd-yyyy</Text>
                                    </View>
                                </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{width: deviceWith * 0.85, borderBottomWidth: 0.5, borderBottomColor: '#6e799d', marginTop: 20,
                                alignItems: 'center', flexDirection: 'row', height: 30}}>
                                <Image source={phoneProfileIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                                <AutoGrowingTextInput multiline={false} placeholder={Strings.hintPhoneNumber} placeholderTextColor = 'black'
                                        underlineColorAndroid ='transparent'
                                        minHeight={40}
                                        maxHeight={45}
                                        style={{marginLeft: Dimens.dp_10, flex: 1, fontSize: 14,
                                        marginBottom: Platform.OS == 'android' ? 10 : 0}}></AutoGrowingTextInput>
                            </View>
                            <View style={{width: deviceWith * 0.85, borderBottomWidth: 0.5, borderBottomColor: '#6e799d', marginTop: 20,
                                alignItems: 'center', flexDirection: 'row', height: 30}}>
                                <Image source={mailIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                                <AutoGrowingTextInput multiline={false} placeholder={Strings.hintEmail} placeholderTextColor = 'black'
                                        underlineColorAndroid ='transparent'
                                        minHeight={40}
                                        maxHeight={45}
                                        style={{marginLeft: Dimens.dp_10, flex: 1, fontSize: 14,
                                        marginBottom: Platform.OS == 'android' ? 10 : 0}}></AutoGrowingTextInput>
                            </View>
                            <View style={{width: deviceWith * 0.85, borderBottomWidth: 0.5, borderBottomColor: '#6e799d', marginTop: 20,
                                alignItems: 'center', flexDirection: 'row', height: 30}}>
                                <Image source={locationIcon} style={{width: 20, height: 20, resizeMode: 'contain'}}></Image>
                                <AutoGrowingTextInput multiline={false} placeholder={Strings.hintAdress} placeholderTextColor = 'black'
                                        underlineColorAndroid ='transparent'
                                        minHeight={40}
                                        maxHeight={45}
                                        style={{marginLeft: Dimens.dp_10, flex: 1, fontSize: 14,
                                        marginBottom: Platform.OS == 'android' ? 10 : 0}}></AutoGrowingTextInput>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: Dimens.dp_50, height: deviceHeight * 0.25,
                                    alignItems: 'center', justifyContent: 'center', marginBottom: deviceWith / 5}}>
                                <Button rounded onPress ={() => {alert(1)}}
                                    style={{backgroundColor: '#b51218', width: deviceWith * 0.6,
                                    height: deviceWith * 0.1,
                                    justifyContent: 'center', borderRadius: 10}}><Text style={{color: 'white'}}>{Strings.textSave.toLocaleUpperCase()}</Text></Button>
                            </View>
                        </View>
                        
                    </Content>
                </View>
            </Container>
        );
    }

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
    };
    _hideDateTimePicker() {
         this.setState({ isDateTimePickerVisible: false });
    }
    _showDateTimePicker() {
        this.setState({ isDateTimePickerVisible: true });
    }
}