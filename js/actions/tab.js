export const CURRENT_TAB = "CURRENT_TAB";
export const STATUS_TAB = "STATUS_TAB";

export function setCurrentTab(isCurrentTab = 'home') {
    return {
        type: CURRENT_TAB,
        isCurrentTab: isCurrentTab
    };
}

export function setStatusTab(isStatusTab = false) {
    return {
        type: STATUS_TAB,
        isStatusTab: isStatusTab
    };
}