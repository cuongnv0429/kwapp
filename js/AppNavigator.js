
import React, { Component } from 'react';
import { BackAndroid, StatusBar, NavigationExperimental, Platform, 
  Text, Image, View} from 'react-native';
import { connect } from 'react-redux';
import { StyleProvider, variables, Drawer } from 'native-base';
import { actions } from 'react-native-navigation-redux-helpers';
import { Router, Scene } from 'react-native-router-flux';
import {Actions, ActionConst} from 'react-native-router-flux';

import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { closeDrawer } from './actions/drawer';
import { setStatusTab } from './actions/tab';

import Login from './components/login/';
import CheckIn from './components/checkin';
import DashBoard from './components/dashboard';
import ForgotPassword from './components/forgotpassword';
import Attendance from './components/attendance';
import Schedule from './components/schedule';
import ScheduleDetail from './components/scheduledetail';
import Review from './components/review';
import Payroll from './components/payroll';
import Profile from './components/profile';
import ChangePassword from './components/changepassword';
import EditProfile from './components/editprofile';
import CheckOut from './components/checkout';
import Absent from './components/absent';
import AbsentDetail from './components/absentdetail';
import DashboardSupervisor from './components/dashboardsupervisor';
import TeacherOffToday from './components/teacherofftoday';
import TeacherOffTodayDetail from './components/teacherofftodaydetail';
import NotCheck from './components/listnotcheck';
import NotCheckReview from './components/notcheckreview';
import NotCheckSubmit from './components/notchecksubmit';
import Today from './components/listtoday';
import Teachers from './components/listteacher';
import TimeSheet from './components/timesheet';
import TimeSheetDetail from './components/timesheetdetail';

const homeIcon = require('../img/home_icons.png');
const calenderIcon = require('../img/calendar_icons.png');
const formIcon = require('../img/form_icons.png');
const menu = require('../img/menu.png');

const {
  popRoute,
} = actions;

const RouterWithRedux = connect()(Router);

const {
  CardStack: NavigationCardStack,
} = NavigationExperimental;

const TabIcon = ({selected, title}) => {
    var icon;
    switch(title) {
      case 'Home':
        icon = homeIcon;
        break;
      case 'Calendar':
        icon = calenderIcon;
        break;
      case 'Form':
        icon = formIcon;
        break;
    }
    return (
        <Image source={icon} style={{width: 24, height: 24, tintColor: selected ? 'white' : '#859ce4', resizeMode: 'contain'}}></Image>
    );
}

class AppNavigator extends Component {

  static propTypes = {
    drawerState: React.PropTypes.string,
    popRoute: React.PropTypes.func,
    closeDrawer: React.PropTypes.func,
    themeState: React.PropTypes.string,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
      routes: React.PropTypes.array,
    }),
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      const routes = this.props.navigation.routes;

      if (routes[routes.length - 1].key === 'home') {
        return false;
      }

      this.props.popRoute(this.props.navigation.key);
      return true;
    });
  }

  componentDidUpdate() {
    // if (this.props.drawerState === 'opened') {
    //   this.openDrawer();
    // }

    // if (this.props.drawerState === 'closed') {
    //   this._drawer._root.close();
    // }
  
  }

  popRoute() {
    this.props.popRoute();
  }

  openDrawer() {
    this._drawer._root.open();
  }

  closeDrawer() {
    if (this.props.drawerState === 'opened') {
      this.props.closeDrawer();
    }
  }
  render() {
    return (
      <StyleProvider style={getTheme((this.props.themeState === 'material') ? material : undefined)}>
          <RouterWithRedux>
            <Scene key="root">
              <Scene key="login" component={Login} hideNavBar initial={true} />
              <Scene key="checkin" component={CheckIn}/>
              <Scene key="forgotpassword" component={ForgotPassword}/>
              <Scene key="changepassword" component={ChangePassword}/>
              <Scene key="profile" component={Profile} hideNavBar/>
              <Scene key="editprofile" component={EditProfile} hideNavBar/>
              <Scene key="review" component={Review} hideNavBar/>
              <Scene key="dashboardsupervisor" component={DashboardSupervisor} hideNavBar/>
              <Scene key="teacherofftoday" component={TeacherOffToday} hideNavBar/>
              <Scene key="teacherofftodaydetail" component={TeacherOffTodayDetail} hideNavBar/>
              <Scene key="notcheck" component={NotCheck} hideNavBar/>
              <Scene key="notcheckreview" component={NotCheckReview} hideNavBar/>
              <Scene key="notchecksubmit" component ={NotCheckSubmit} hideNavBar/>
              <Scene key="today" component={Today} hideNavBar/>
              <Scene key="teacher" component={Teachers} hideNavBar/>
              <Scene key="timesheet" component={TimeSheet} hideNavBar/>
              <Scene key="timesheetdetail" component ={TimeSheetDetail} hideNavBar/>
              <Scene
                    key='tabbar'
                    tabs ={true}
                    tabBarStyle={{backgroundColor: '#18285a'}}
                    navigationBarStyle={{backgroundColor: '#18285a'}}
                    hideTabBar={this.props.isStatusTab}>
                    <Scene key='home' title='Home' icon={TabIcon}
                      hideNavBar>
                      <Scene key='dashboard'
                            component={DashBoard}
                            title='DashBoard'/>
                      <Scene key='checkin'
                            component={CheckIn}
                            title='Checkin'/>
                      <Scene key="checkout" 
                            component={CheckOut}
                            title='Checkout'/>
                      <Scene key='attendance'
                            component={Attendance}
                            title='Attendance'/>
                      
                    </Scene>
                    <Scene key='calendar' title='Calendar' icon={TabIcon}
                      hideNavBar>
                      <Scene key='schedule'
                            component={Schedule}
                            title='Schedule'/>
                      <Scene key="scheduledetail" 
                            component={ScheduleDetail}/>
                      <Scene key="payroll" 
                            component={Payroll}/>
                    </Scene>
                    <Scene key='form' title='Form' icon={TabIcon}
                      hideNavBar>
                      <Scene key='absent'
                            component={Absent}
                            title='Absent'/>
                      <Scene key='absentdetail'
                            component={AbsentDetail}
                            title='AbsentDetail'/>
                    </Scene>
              </Scene>
            </Scene>
          </RouterWithRedux>
      </StyleProvider>
    );
  }
}

const bindAction = dispatch => ({
  closeDrawer: () => dispatch(closeDrawer()),
  popRoute: key => dispatch(popRoute(key)),
  setStatusTab: (isStatusTab) => dispatch(setStatusTab(isStatusTab))
});

const mapStateToProps = state => ({
  drawerState: state.drawer.drawerState,
  themeState: state.drawer.themeState,
  navigation: state.cardNavigation,
  isStatusTab: state.tab.isStatusTab
});

export default connect(mapStateToProps, bindAction)(AppNavigator);
