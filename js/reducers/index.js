
import { combineReducers } from 'redux';

import drawer from './drawer';
import routes from './routes';
import cardNavigation from './cardNavigation';
import tab from './tab';

export default combineReducers({

  drawer,
  cardNavigation,
  routes,
  tab
});
