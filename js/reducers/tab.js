import {CURRENT_TAB} from '../actions/tab.js';
import {STATUS_TAB} from '../actions/tab.js';
const DEFAULT_STATE = {isCurrentTab: 'home', isStatusTab: false};

export default function(state = DEFAULT_STATE, action) {
    switch(action.type) {
        case CURRENT_TAB:
            return {
                ...state,
                isCurrentTab: action.isCurrentTab
            };
        case STATUS_TAB:
            return {
                ...state,
                isStatusTab: action.isStatusTab
            };
        default :
            return state;
    }
}